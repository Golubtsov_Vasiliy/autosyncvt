#ifndef STRINGUTILS_H
#define STRINGUTILS_H

#include <string>
#include <sstream>
#include <list>
#include <iterator>
#include <algorithm>
#include <cctype>
#include <locale>
#include <vector>

class StringUtils
{
public:
    #ifdef _MSC_VER
        static const char curSplitDirSymbol;
        static const std::string curSplitDirString;
    #elif defined __GNUC__
        static const char curSplitDirSymbol;
        static const std::string curSplitDirString;
    #endif

    struct RecognizedWord{
        double _posBegin;
        double _posEnd;
        std::wstring _recognizedString;
    };
    typedef std::list <RecognizedWord> RecognizedString;

    static inline int32_t findFirstNotAlpha(std::wstring str, int32_t firstPos = 0){
        return StringUtils::findFirst(str, firstPos, [](const wchar_t& ch){
            return iswalpha(ch) == false;
        });
    }

    static inline int32_t findFirstAlpha(std::wstring str, int32_t firstPos = 0){
        return StringUtils::findFirst(str, firstPos, [](const wchar_t& ch){
            return iswalpha(ch);
        });
    }

    template <typename _Predicate>
    static inline int32_t findFirst(std::wstring str, int32_t firstPos, _Predicate eq){
        auto begin = str.begin();
        if (firstPos != 0)
            begin = std::next(begin, firstPos);
        auto findedI = std::find_if(begin, str.end(), eq);
        if (findedI == str.end())
            return std::wstring::npos;
        int32_t findedPos = std::distance(begin, findedI) + firstPos;
        return findedPos;
    }

    static inline RecognizedString summ(const RecognizedString& left, const RecognizedString& right){
        RecognizedString rez = left;
        rez.insert(rez.end(), left.begin(), left.end());
        return rez;
    }

    static inline void offset(RecognizedString& recognizedString, double offset){
        for (RecognizedWord& word : recognizedString)
        {
            word._posBegin += offset;
            word._posEnd += offset;
        }
    }
    static inline std::wstring s2ws(const std::string& s) {
        std::string curLocale = setlocale(LC_ALL, "");
        const char* _Source = s.c_str();
        size_t _Dsize = mbstowcs(NULL, _Source, 0) + 1;
        wchar_t *_Dest = new wchar_t[_Dsize];
        wmemset(_Dest, 0, _Dsize);
        mbstowcs(_Dest,_Source,_Dsize);
        std::wstring result = _Dest;
        delete []_Dest;
        setlocale(LC_ALL, curLocale.c_str());
        return result;
    }
    static inline void fixFloatPoint(std::wstring& s)
    {
    #ifdef _MSC_VER
        exchangeFloatPointToPoint(s);
    #elif defined __GNUC__
        exchangePointToFloatPoint(s);
    #endif
    }
    // Возвращает подвектор с начальной по конечную позицию включительно
    static inline std::vector<std::wstring> getSubVector(const std::vector<std::wstring>& vector, int32_t begin, int32_t end)
    {
        std::vector<std::wstring> subVector;
        int32_t subSize = end-begin+1;
        subVector.reserve(subSize);
        for (int32_t i = begin; i<=end; i++)
        {
            std::wstring cur = vector.at(i);
            subVector.push_back(cur);
        }
        return subVector;
    }
    // если words < 0, берём последние words слов. Если больше первые words
    static inline std::vector<std::wstring> getSubVector(const std::vector<std::wstring>& vector, int32_t words)
    {
        if (words < 0)
        {
            int32_t vectorSize = vector.size();
            int32_t firstPos = vectorSize-abs(words);
            if (firstPos < 0)
                firstPos = 0;
            return StringUtils::getSubVector(vector, firstPos, vectorSize-1);
        }
        else
            return StringUtils::getSubVector(vector, 0, words);
    }

    template<typename continer>
    static inline int32_t lengthBeforeSplit(const continer& cont){
        int32_t fullLength = getLength(cont);
        int32_t length = fullLength - cont.size() + 1;
        return length;
    }

    template<typename continer>
    static inline int32_t getLength(const continer& cont){
        int32_t summLength = 0;
        for (auto el : cont)
            summLength += el.length();
        return summLength;
    }
    static inline std::list<std::wstring> getSubList(const std::list<std::wstring>& list, int32_t begin, int32_t end)
    {
        std::list<std::wstring> subList;
        auto cur = list.begin();
        for (int32_t i = 0; i<=end; i++, cur++)
            if (i >= begin)
                subList.push_back(*cur);
        return subList;
    }
    // .. Распознанную строку
    static inline RecognizedString getSubRecognizedString(const RecognizedString& recStr, double begin, double end){
        RecognizedString subStr;
        for (RecognizedWord recWord : recStr)
        {
            double wordBegin = recWord._posBegin;
            //double wordEnd = recWord._posEnd;
            if (wordBegin > end)
                break;
            if (wordBegin > begin)
                subStr.push_back(recWord);
        }
        return subStr;
    }
    // Корень слова - для сравнений
    static inline void toRoot(std::wstring& str){
        for (auto &symbol : str)
            symbol = towlower(symbol);
        StringUtils::deleteNotAlpha(str);
    }
    static inline void deleteNotAlpha(std::wstring& str){
        for (int32_t i=0; i<str.length();)
        {
            wchar_t curSimbol = str[i];
            if (iswalpha(curSimbol)==false /*&& curSimbol != '\''*/)
                str = str.erase(i, 1);
            else
                i++;
        }
    }

    // TODO ВАЖНО: Заменить сразу на разбиение по словам, а эту в приват
    template<typename StringType, typename charType>
    static inline std::list<StringType> split(const StringType &s, charType spaceChar) {
        std::list<StringType> elems;
        split(s, spaceChar, std::back_inserter(elems));
        return elems;
    }
    // Нужна для удаления скобок в распознанных словах
    static inline std::string removeBrackets(const std::string& str){
        auto breakedPos = str.find("(");
        if (breakedPos == std::string::npos)
            return str;
        std::string rezStr = str.substr(0, breakedPos); // так как скобки у нас только после полезной части
        return rezStr;
    }

    template<typename StringType, typename charType>
    static inline StringType join(const std::list<StringType>& list, charType spaceChar){
        StringType rezJoin;
        for (StringType part : list)
            rezJoin += part + spaceChar;
        if (rezJoin.empty() == false)
            rezJoin.pop_back();
        return rezJoin;
    }
    // trim from both ends (in place)
    template<typename StringType>
    static inline void trim(StringType &s) {
        ltrim(s);
        rtrim(s);
    }
    static inline int32_t getRecognizedLength(const StringUtils::RecognizedString& recognized)
    {
        int32_t length;
        for (StringUtils::RecognizedWord word : recognized)
        {
            std::wstring wordString = word._recognizedString;
            length += wordString.length();
        }
        return length;
    }
    static inline std::list<std::wstring> toStringList(const StringUtils::RecognizedString& recognized)
    {
        std::list<std::wstring> rezList;
        toStringContinerType(recognized, rezList);
        return rezList;
    }
    static inline std::vector<std::wstring> toStringVector(const StringUtils::RecognizedString& recognized)
    {
        std::vector<std::wstring> rezVector;
        rezVector.reserve(recognized.size());
        toStringContinerType(recognized, rezVector);
        return rezVector;
    }
    static inline std::vector<StringUtils::RecognizedWord> toVector(const StringUtils::RecognizedString& recognized)
    {
        std::vector<StringUtils::RecognizedWord> rezVector;
        rezVector.reserve(recognized.size());
        toContinerType(recognized, rezVector);
        return rezVector;
    }

    static inline bool isEquils(const StringUtils::RecognizedWord& R, const std::wstring& r){
        if (R._recognizedString == r)
            return true;
        return false;
    }

protected:
    StringUtils() = delete;
    template<typename Continer>
    static inline void toContinerType(const StringUtils::RecognizedString& recognized, Continer& out){
        for (StringUtils::RecognizedWord word : recognized)
            out.push_back(word);
    }
    template<typename Continer>
    static inline void toStringContinerType(const StringUtils::RecognizedString& recognized, Continer& out){
        for (RecognizedWord word : recognized)
        {
            std::wstring wordString = word._recognizedString;
            out.push_back(wordString);
        }
    }

    template<typename Out>
    static void split(const std::wstring &s, wchar_t spaceChar, Out result) {
        std::wstringstream ss;
        ss.str(s);
        std::wstring item;
        while (std::getline(ss, item, spaceChar)) {
            *(result++) = item;
        }
    }
    template<typename Out>
    static void split(const std::string &s, char spaceChar, Out result) {
        std::stringstream ss;
        ss.str(s);
        std::string item;
        while (std::getline(ss, item, spaceChar)) {
            *(result++) = item;
        }
    }

    // trim from start (in place)
    template<typename StringType>
    static inline void ltrim(StringType &s) {
        //std::locale loc;
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
            return !isspace(ch);
        }));
    }

    // trim from end (in place)
    template<typename StringType>
    static inline void rtrim(StringType &s) {
        //std::locale loc;
        s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
            return !isspace(ch);
        }).base(), s.end());
    }

    // trim from start (copying)
    static inline std::string ltrim_copy(std::string s) {
        ltrim(s);
        return s;
    }

    // trim from end (copying)
    static inline std::string rtrim_copy(std::string s) {
        rtrim(s);
        return s;
    }

    // trim from both ends (copying)
    static inline std::string trim_copy(std::string s) {
        trim(s);
        return s;
    }
    // Заменяет дочку на плавующую точку
    static inline void exchangePointToFloatPoint(std::wstring& s)
    {
        auto pointPos = s.find(L'.');
        s[pointPos] = L',';
    }
    static inline void exchangeFloatPointToPoint(std::wstring& s)
    {
        auto pointPos = s.find(L',');
        s[pointPos] = L'.';
    }
};

#endif // STRINGUTILS_H
