#ifndef FILEWORKER_H
#define FILEWORKER_H

#include <iostream>
#include <stdio.h>
#include "stringutils.h"
#include "exceptionHandler.h"

class FileWorker
{
public:
    static inline std::string pathToFile(const std::string& file)
    {
        std::list<std::string> fileSplit = StringUtils::split(file, StringUtils::curSplitDirSymbol);
        fileSplit.pop_back();
        if (fileSplit.empty())
        {
            std::string curDir;
            curDir.push_back(StringUtils::curSplitDirSymbol);
            return curDir;
        }
        std::string rezPath = StringUtils::join(fileSplit, StringUtils::curSplitDirSymbol);
        return rezPath;
    }
    static inline std::string fileName(const std::string& file)
    {
        std::list<std::string> fileSplit = StringUtils::split(file, StringUtils::curSplitDirSymbol);
        std::string rezName = fileSplit.back();
        return rezName;
    }
    static inline std::string baseName(const std::string& file)
    {
        std::string name = fileName(file);
        std::string base = exchangeExtension(name, "");
        return base;
    }
    //                                                                          ".txt"
    static inline std::string exchangeExtension(const std::string& file, std::string newExtension)
    {
        std::string rezFile = file;
        int32_t length = file.length();
        auto extPos = file.find_last_of('.');
        if (extPos!=std::string::npos)
        {
            int32_t newLength = extPos;
            rezFile = rezFile.substr(0, newLength);
        }
        rezFile += newExtension;
        return rezFile;
    }
    static inline bool exists(const std::string& name){
        if (FILE *file = fopen(name.c_str(), "r"))
        {
            fclose(file);
            return true;
        }
        else
            return false;
    }
    static inline void remove(const std::string& name)
    {
        if (std::remove(name.c_str()) != 0)
            ExceptionHandler::warning("We can\' delete file " + name);
    }
    static inline bool copy(const std::string& name, const std::string& copy_name)
    {

        return false;
    }
    static inline bool rename(const std::string& name, const std::string& new_name)
    {
        if (std::rename(name.c_str(), new_name.c_str()) != 0)
            ExceptionHandler::error("We can\' rename file " + name + " in " + new_name);
        return false;
    }

protected:
    FileWorker() = delete;
};

#endif // FILEWORKER_H
