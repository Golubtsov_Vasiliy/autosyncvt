#ifndef DIRWORKER_H
#define DIRWORKER_H

#include <string>
#include <list>
#include "exceptionHandler.h"
#include "fileworker.h"

#ifdef _MSC_VER
#include <direct.h>
#include <windows.h>
#elif defined __GNUC__
#include <dirent.h>
#include <sys/types.h> // Unix
#include <sys/stat.h>
#endif

class DirWorker
{
public:
#ifdef _MSC_VER
    static const std::string _curDirString;
#elif defined __GNUC__
    static const std::string _curDirString;
#endif
    static inline std::string getCurrentPath()
    {
        if (_curentPath.empty())
        {
            ExceptionHandler::error("We don\'t init curent path");
        }
        return _curentPath;
    }
    static inline std::string getScriptPath()
    {
        #ifdef _MSC_VER
            return "\\scripts\\";
        #elif defined __GNUC__
            return "/scripts/";
        #endif
    }

    // TODO Перенести в FileWorker
    static inline std::string absolutePath(const std::string& name){
        std::string curPath = getCurrentPath();
        if (name.find(curPath) == 0)
            return name;
        std::string rezPath = curPath + StringUtils::curSplitDirSymbol + name;
        return rezPath;
    }

    static inline std::list<std::string> entryInfoList(const std::string& dir, const std::list<std::string>& ext)
    {
        std::list<std::string> rezList;

        return rezList;
    }
    static inline std::list<std::string> entryInfoList(const std::list<std::string>& ext)
    {
        std::list<std::string> rezList;

        return rezList;
    }
    static inline std::list<std::string> entryInfoList(const std::string& ext)
    {
        std::list<std::string> rezList;

        return rezList;
    }
    static inline void mkDir(const std::string& path)
    {
        #ifdef _MSC_VER
            _mkdir(path.data());
        #elif defined __GNUC__
            mkdir(path.data(), 0777);
        #endif
    }
    static inline void clearDir(const std::string& path)
    {

    }    
    static inline void initDurWorker(int argc, char *argv[]){
        std::string currentPath = FileWorker::pathToFile(argv[0]);
        setCurrentPath(currentPath);
    }
    static inline void setCurrentPath(const std::string& curPath){
        _curentPath = curPath;
    }
    static inline std::string relativePath(const std::string& path, const std::string& rootPath){
        auto firstChar = path.find(rootPath);
        if (firstChar!=0)
            ExceptionHandler::critical("We don\'t worl with absolute paths " + path + " " + rootPath);
        auto rLength = path.size() - rootPath.size();
        if (rLength <= 0)
            ExceptionHandler::critical("Problem with path " + path);
        std::string rPath = path.substr(rootPath.length() + 1, rLength - 1); // +1 -1 - дополнительный слеш
        return rPath;
    }
    static inline bool exists(const std::string& name){    
        #if defined _MSC_VER
            DWORD ftyp = GetFileAttributesA(name.c_str());
        if (ftyp == INVALID_FILE_ATTRIBUTES)
            ExceptionHandler::error("Problem with directory " + name);
        if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
            return true;
        return false;
        #elif defined __GNUC__
        mkdir(name.data(), 0777);
            DIR* dir = opendir("mydir");
            if (dir)
            {
                closedir(dir);
                return true;
            }
            else if (ENOENT == errno)
                return false;
            else
                ExceptionHandler::error("Problem with directory " + name);

        #endif
        return 0;
    }

protected:
    static std::string _curentPath;

    DirWorker() = delete;



};



#endif // DIRWORKER_H
