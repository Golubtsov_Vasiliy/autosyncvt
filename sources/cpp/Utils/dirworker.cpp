#include "dirworker.h"

std::string DirWorker::_curentPath = "";

#ifdef _MSC_VER
    const std::string DirWorker::_curDirString = "\\";
#elif defined __GNUC__
    const std::string DirWorker::_curDirString = "/";
#endif
