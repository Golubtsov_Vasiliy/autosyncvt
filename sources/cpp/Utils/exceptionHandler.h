#ifndef EXCEPTIONHANDLER_H
#define EXCEPTIONHANDLER_H

#include <string>
#include <iostream>

class ExceptionHandler
{
public:
    ExceptionHandler() = delete;

    static inline void critical(const std::string& mess){
        std::cout << "CRITICAL ERROR: " << mess << "\n";
        exit(1);
    }
    static inline void error(const std::string& mess){
        std::cout << "ERROR: " << mess << "\n";
    }
    static inline void warning(const std::string& mess){
        std::cout << "WARNING: " << mess << "\n";
    }
    static inline void log(const std::wstring& mess){
        std::wcout << L"Log: " << mess << "\n";
    }
};

#endif // EXCEPTIONHANDLER_H
