#include "stringutils.h"

#ifdef _MSC_VER
    const char StringUtils::curSplitDirSymbol = '\\';
    const std::string StringUtils::curSplitDirString = "\\";
#elif defined __GNUC__
    const char StringUtils::curSplitDirSymbol = '/';
    const std::string StringUtils::curSplitDirString = "/";
#endif
