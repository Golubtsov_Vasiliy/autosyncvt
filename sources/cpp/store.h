#ifndef STORE_H
#define STORE_H

#include <string>
#include <cstdint>
#include <memory>
#include "Utils/dirworker.h"

// Базовый класс хранилища данных
template <class StoreType> // Нужна для фабрики
class Store
{
public:
    virtual std::string fileUrl() const = 0;
    virtual void setFileUrl(const std::string& url) = 0;

    typedef std::shared_ptr <StoreType> PTR;
    static PTR factoryMethod()
    {
        PTR rezPtr = std::shared_ptr <StoreType> (new StoreType());
        return rezPtr;
    }
    std::string getHash()
    {
        std::string url = fileUrl();
        std::string md5 = getHash(url);
        return md5;
    }
    std::string toString(const std::string& url, std::string curPath) const
    {
        std::string stringUrl =  DirWorker::relativePath(url, curPath);
        return stringUrl;
    }
    std::string toString(std::string curPath) const
    {
        std::string localUrl = fileUrl();
        std::string url = DirWorker::absolutePath(localUrl);
        std::string fileString = toString(url, curPath);
        return fileString;
    }
    std::string getFileName()
    {
        std::string fileName = fileUrl().fileName();
        return fileName;
    }
    // потдерживаются только относительные пути
    // в дирректории ниже исполнительного файла
    void fromString(std::string storeString, std::string curPath)
    {
        //std::string fileUrl;
        //storeString = curPath + "/" + storeString;
        //QFileInfo localFile(storeString);
        //std::string apsoluteStringUrl = localFile.absoluteFilePath();
        //fileUrl = QUrl::fromLocalFile(apsoluteStringUrl);
        std::string fullPathStoreString = curPath + StringUtils::curSplitDirString + storeString;
        setFileUrl(fullPathStoreString);
    }
    virtual ~Store() {
       // qDebug() << "~Store()";
    }
protected:
    std::string getHash(std::string url)
    {
        return std::string("MD5");
    }
    Store(){
        //qDebug() << "Store()";
    }
};


#endif // STORE_H
