#ifndef SOUNDFRAGMENT_H
#define SOUNDFRAGMENT_H

#include <cstdint>
#include "fragment.h"
#include "soundstore.h"

class SoundFragment : public Fragment <SoundFragment, SoundStore::PTR, double>
{
public:
    virtual ~SoundFragment() {
       // qDebug() << "~SoundFragment()";
    }
protected:
    friend class Fragment <SoundFragment, SoundStore::PTR, double>;
    SoundFragment() = delete;
    // В глобальных координатах
    SoundFragment(double begin, double end, SoundStore::PTR source)
        : Fragment(begin, end, source) {
      //  qDebug() << "SoundFragment()";
    }
};

#endif // SOUNDFRAGMENT_H
