#include "logic.h"

Logic::Logic(void)
    :BaseLogic()
{
   // zeroBind.sound = SoundFragment::factoryMethod(-1.0f, -1.0f, nullptr);
    //zeroBind.text = TextFragment::factoryMethod(-1, -1, nullptr);
   // tempBind.sound = SoundFragment::factoryMethod(-1.0f, -1.0f, nullptr);
   // tempBind.text = TextFragment::factoryMethod(-1, -1, nullptr);
}

void Logic::bindLogicHanding()
{
    Bind lastBind = tempBind;
    // Обрабатываем текстовые конци биндов
    for (auto& bind : _bindVector)
    {
        auto curText = bind.text;
        auto lastText = lastBind.text;
        int32_t curBegin = curText->begin();
        lastText->setEnd(curBegin);
        lastBind = bind;
    }
    lastBind = tempBind;
    // Обрабатываем текстовые конци биндов
    for (auto& bind : _bindVector)
    {
        auto curSound = bind.sound;
        auto lastSound = lastBind.sound;
        double curBegin = curSound->begin();
        lastSound->setEnd(curBegin);
        lastBind = bind;
    }
}

StringUtils::RecognizedString Logic::getRecognizedString(const std::string& fileName) const
{
    return _recognizedStrings.at(fileName);
}

StringUtils::RecognizedString Logic::recognizedInBind(int32_t bindNumber) const
{
    assert(_recognizedInBind.find(bindNumber) != _recognizedInBind.end());
    return _recognizedInBind.at(bindNumber);
}

StringUtils::RecognizedString Logic::getRecognizedIn(double begin, double end) const
{
    StringUtils::RecognizedString rezStr;
    for (auto item : _recognizedStrings)
    {
        StringUtils::RecognizedString recStr = item.second;
        double strBegib = recStr.front()._posBegin;
        double strEnd = recStr.back()._posEnd;
        if (strBegib > begin && strEnd < end)
            rezStr = StringUtils::summ(rezStr, recStr);
    }
    return rezStr;
}

void Logic::addRecognizedInBinds()
{
    const double Eps = 0.1;
    for (int32_t i = 0 ; i<_bindVector.size(); i++)
    {
        Bind bind = _bindVector.at(i);
        SoundFragment::PTR sound = bind.sound;
        double begin = sound->begin() - Eps;
        double end = sound->end();
        if (begin < 0)
            begin = 0;
        if (i != _bindVector.size() - 1)
            end += Eps;
        StringUtils::RecognizedString recInBind = getRecognizedIn(begin, end);
        addRecognizedInBind(recInBind, i);
    }
}

void Logic::addRecognizedInBind(const StringUtils::RecognizedString& rec, int32_t bindNumber)
{
    if (bindNumber < 0)
        bindNumber = _bindVector.size() - 1;
    _recognizedInBind[bindNumber] = rec;
}

void Logic::makeBind(TextFragment::PTR text, SoundFragment::PTR sound)
{
    Bind b;
    b.text = text;
    b.sound = sound;
    addInBindList(b);
}

void Logic::save()
{
    writeInFile(_curBndFileName, _lastOpenedTextStore, _lastOpenedSoundStore);
}

bool Logic::haveRecognizedString()
{
    return _recognizedStrings.empty() == false;
}

std::map <std::string, StringUtils::RecognizedString> Logic::getRecognizedStrings() const
{
    return _recognizedStrings;
}

std::map <std::string, double> Logic::getRecognizedStringBeginList() const
{
    return _recognizedStringPosBegin;
}

std::map <std::string, double> Logic::getRecognizedStringEndList() const
{
    return _recognizedStringPosEnd;
}


//void Logic::writeInFile(const std::string& fileName)
//{
//    writeInFile(fileName, _lastOpenedTextStore, _lastOpenedSoundStore);
//}
