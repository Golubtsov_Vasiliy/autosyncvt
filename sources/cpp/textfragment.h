#ifndef TEXTFRAGMENT_H
#define TEXTFRAGMENT_H

#include <cstdint>
#include "fragment.h"
#include "textstore.h"

class TextFragment : public Fragment <TextFragment, TextStore::PTR, int32_t>
{
public:
    // i - в локальных координатах
    char getSymbol(int32_t i) const;
    std::wstring getString() const;
    std::vector<std::wstring> getWordList() const;
    int32_t getFragmentLength() const;
    static bool isEquils(std::wstring left, std::wstring right);

    virtual ~TextFragment() {}
protected:
    friend class Fragment <TextFragment, TextStore::PTR, int32_t>;
    TextFragment() = delete;
    // В глобальных координатах
    TextFragment(int32_t begin, int32_t end, TextStore::PTR source)
        : Fragment(begin, end, source) { }
};

#endif // TEXTFRAGMENT_H
