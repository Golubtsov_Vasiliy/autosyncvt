#include "textfragment.h"

char TextFragment::getSymbol(int32_t i) const
{
    std::wstring thisString = this->getString();
    return thisString.at(i);
}

std::wstring TextFragment::getString() const
{
    std::wstring subSourceString = _source->getString(_begin, _end);
    return subSourceString;
}

std::vector<std::wstring> TextFragment::getWordList() const
{
    int32_t beginWord = _source->wordNumberOnPos(_begin);
    int32_t endWord = _source->wordNumberOnPos(_end);
    int32_t wordSize = endWord - beginWord + 1;
    std::vector<std::wstring> wordList;
    wordList.reserve(wordSize);
    for (int32_t i_curWord = beginWord; i_curWord < endWord; i_curWord++)
    {
        std::wstring curWord = _source->word(i_curWord);
        wordList.push_back(curWord);
    }
    return wordList;
}

int32_t TextFragment::getFragmentLength() const
{
    return _end - _begin;
}

bool TextFragment::isEquils(std::wstring left, std::wstring right)
{
    StringUtils::toRoot(left);
    StringUtils::toRoot(right);
    return left == right;
}
