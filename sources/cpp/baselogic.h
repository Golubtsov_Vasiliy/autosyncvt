#ifndef BASELOGIC_H
#define BASELOGIC_H
#include <memory>
#include "soundfragment.h"
#include "textfragment.h"

class BaseLogic
{
public:
    struct Bind{
        TextFragment::PTR text;
        SoundFragment::PTR sound;
    };

    void writeInFile(TextStore::PTR , SoundStore::PTR);
    void writeInFile(const std::string& fileName, TextStore::PTR , SoundStore::PTR);
    void readFromFile(const std::string& fileName, TextStore::PTR, SoundStore::PTR);

    void addRecognizedString(const StringUtils::RecognizedString&, SoundFragment::PTR sound, std::string fileName = "");
    void addRecognizedString(const StringUtils::RecognizedString&, double beginSound, double endSound, std::string fileName = "");

    void clear(bool clearRecognized);

    std::vector <Bind> getBindVector() const;
    Bind getBind(int32_t bindNumber) const;
    std::string getCurBndFileName() const;
    int32_t getBindVectorSize() const;
    int32_t getBindTextPos(int32_t pos) const;

    BaseLogic();
protected:
    // Расширения потдерживаемых форматов файлов
    // Нужны для утановления типа открываемого файла или коректности охраняемого url
    const std::list<std::wstring> textFileExtension = {L".txt"};
    const std::list<std::wstring> soundFileExtension = {L".wav"};
    const std::list<std::wstring> bindFileExtension = {L".bind"};

    // Константы для записи и парсинга файлов
    const std::wstring par_SoundStore = std::wstring(L"soundStore");
    const std::wstring par_TextStore = std::wstring(L"textStore");
    const std::wstring par_TextStoreHash = std::wstring(L"MD5TextStore");
    const std::wstring par_SoundStoreHash = std::wstring(L"MD5SoundStore");
    const std::wstring par_Bind = std::wstring(L"Bind");
    const std::wstring par_RecognizedString = std::wstring(L"RecognizedString");
    const std::wstring par_RecognizedStringPos = std::wstring(L"RecognizedStringPos");

    std::string _curBndFileName;
    SoundStore::PTR _lastOpenedSoundStore;
    TextStore::PTR _lastOpenedTextStore;

    // Возвращается если не нашли подходящего бинда
    // Иницилизирован в конструкторе
    Bind zeroBind; // Не стоит модифицировать, нейтрален отнасительно суммирования + используеться в лямдах
    Bind tempBind; // служит как временная переменная которую можно поменять, не на что не повличет

    // Упорядоченное множество биндов
    // Нужно для подсветки и дихотомического поиска соответствующего выделения
    std::vector <Bind> _bindVector;

    // Строка состоящая из слов бинда
    //std::vector<std::wstring> getBindStringVector(const Bind&) const;
    //std::vector<std::wstring> getBindStringRootVector(const Bind&) const; // Корни слова

    std::map <std::string, StringUtils::RecognizedString> _recognizedStrings; // Распознаные строки из файла, нужны для быстрого перепросчёта
    std::map <std::string, double> _recognizedStringPosBegin; // позициия в звуковом файле для каждого элемента _recognizedStrings
    std::map <std::string, double> _recognizedStringPosEnd;
    std::map <int32_t, StringUtils::RecognizedString> _recognizedInBind; // Служит для поправки биндов. Не верен после поправки.

    // Добавление с сохранением порядка
    void addInBindList(const Bind&);

    std::wstring toString(const Bind &) const;
    void fromString(Bind&, const std::wstring &bindString, SoundStore::PTR sound, TextStore::PTR text) const;
    std::wstring toString(const StringUtils::RecognizedString& str) const;
    void fromString(StringUtils::RecognizedString& str, const std::wstring recognizedStringString) const; // нужна для чтения распозноного текста

    bool isEquils(const Bind& left, const Bind& right) const;
    Bind summ(const Bind& left, const Bind& right) const;
};

#endif // BASELOGIC_H
