#ifndef DATAPREPARATION_H
#define DATAPREPARATION_H

#include <string>
#include <list>
#include <map>
#include <assert.h>
#include <math.h>
#include "scripter.h"
#include "wavworker.h"
#include "../Utils/dirworker.h"

// Нужна чтобы хранить части звуковой дорожки // Атоматически не чиститься
const std::string SPLIT_DIR = "split";

// wav utils c++ - https://github.com/nicolamontecchio

class DataPreparation
{
    // Нужно для соблюдения фиксированной частоты дискретизации
    const int64_t _16k = 16000;
    const int64_t _8k = 8000;
public:
    explicit DataPreparation();

    std::string prepeareWav(const std::string& videoFile);

    void generateNoise_wav(const std::string& fileName, const std::string& noiseWav, int64_t begin, int64_t end); // генерим noiseWav с примером шума из файла

    // Делит файл на несколько маленьких
    // splitSize - время в секундах, определяет размер
    void splitFile(const std::string& fileName, double splitSize, const std::string& outDirrectory = SPLIT_DIR);
    void splitFile(const std::string& fileName, double minSplitSize, double maxSplitSize, const std::string& outDirrectory = SPLIT_DIR);

    // Методы возвращающие результат операций подготовки
    std::list<std::string> getFileNameList() const { return _rezFileNameList; }
    std::map <std::string, double> getFileBeginList() const { return _rezFileBeginOffset; }
    std::map <std::string, double> getFileEndList() const { return _rezFileEndOffset; }

    // Нужно для соблюдения дискретизации
   // bool fileIsCorrect(const QString& fileName);
    static bool clearDir(const std::string& dir);
    static bool copy(const std::string& originalDir, const std::string& copyDir, const std::list<std::string>& mask = std::list<std::string>({"*.*"}));
    static void clearTrash(); // Чистит от мусора папке с программой от временных вав файлов

    void clearDefaultSplitDir();

private:
    WavWorker _wav;
    Scripter _scripter;
    std::list<std::string> _rezFileNameList;
    std::map <std::string, double> _rezFileBeginOffset;
    std::map <std::string, double> _rezFileEndOffset;
    const int64_t _noiseFileSize = 8000; // размер файла с шумом, для построения модели шума, должно быть кратно четырём
    // Позиции в которых ищем шум
    const int64_t _noiseWindowBegin = 30 * _16k;
    const int64_t _noiseWindowEnd = 60 * _16k; // Считаем что файл хотябы 60 секунд

    const std::string _noise_wav = DirWorker::getCurrentPath() + StringUtils::curSplitDirString + "Scripts" + StringUtils::curSplitDirString + "noise.wav";

    // Нужна для разбиения файла на части
    std::string formNPurtName(const std::string& sourceName, const std::string& ID);

    void writePreparation(WavWorker::Settings&, const std::string& outDirrectory = SPLIT_DIR);

    int64_t getEnergyMin(double* data, int64_t dataSize, int64_t windowSize = 1600);
    double getEnergy(double* data, int64_t begin, int64_t end);

    double getStadyComponent(double* data, int64_t dataSize);
    void deleteStadyComponent(double* data, int64_t dataSize);
    void useMultyWindow(double* data, int64_t dataSize);

    // выделяет новый массив вида [prefix[0], .. , prefix[prefixSize-1], data[0], .. , data[dataSize-1]]
    // при этом исходные data и prefix не удаляетсья
    double* join(double* prefix, int64_t prefixSize, double* data, int64_t dataSize) const;
    // выделяет новый массив вида [source[splitSize] .. source[sourceSize-1]]
    // при этом source не удаляеться
    double* splitBegin(double* source, int64_t sourceSize, int64_t splitSize) const;
};

#endif // DATAPREPARATION_H
