#include "metrics.h"


// Используеться в getDistance, проверяет равенство слов
bool Metrics::isEqils(const std::wstring& string1, const std::wstring& string2)
{
    // Убираем не буквы
    //deleteNotAlpha(lstr1);
    //deleteNotAlpha(lstr2);
    if (string1 == string2)
        return true;
    return false;
}

int32_t Metrics::getLevenshteinDistance(const std::list<std::wstring>& s1, const std::list<std::wstring>& s2)
{
    std::vector <std::vector<int32_t>> mxD; // TODO стоит использовать указатели для лучшего быстродействия
    int32_t stringNumber = s1.size();
    int32_t columnNumber = s2.size();

    mxD.resize(stringNumber);
    for (auto& str : mxD)
        str.resize(columnNumber);

    auto curString = mxD.begin();
    for (int32_t i = 0; i<mxD.size(); i++)
    {
        (*curString)[0] = i;
        curString++;
    }

    auto curColumn = mxD[0].begin();
    for (int32_t j = 0; j<mxD[0].size(); j++)
    {
        *curColumn = j;
        curColumn++;
    }

    auto curStringEl = s1.begin();
    for (int32_t i = 1; i<stringNumber; i++)
    {
        auto curColumnEl = s2.begin();
        for (int32_t j = 1; j<columnNumber; j++)
        {
            int32_t _90_min = std::min(mxD[i-1][j] + 1, mxD[i][j-1] + 1);
            int32_t mFunc = *curColumnEl != *curStringEl;
            int32_t _45_min = mxD[i-1][j-1] + mFunc;
            int32_t D = std::min(_90_min, _45_min);
            mxD[i][j] = D;
            curColumnEl++;
        }
        curStringEl++;
    }
    return mxD[stringNumber-1][columnNumber-1];
}
