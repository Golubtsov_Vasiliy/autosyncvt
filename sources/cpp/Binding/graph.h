#ifndef GRAPH_H
#define GRAPH_H

/*
 * Подправленная версия алгоритма поиска длиннейшего пути - мы ищем маршрут
 * Оригинал http://www.geeksforgeeks.org/find-longest-path-directed-acyclic-graph/
*/

#include <iostream>
#include <stack>
#include <vector>
#include <map>
#include <list>
#include <limits.h>
#include <assert.h>
#define NINF INT32_MIN
using namespace std;

// Graph is represented using adjacency list. Every node of adjacency list
// contains vertex number of the vertex to which edge connects. It also
// contains weight of the edge
class AdjListNode
{
    int32_t v;
    int32_t weight;
public:
    AdjListNode(int32_t _v, int32_t _w)  { v = _v;  weight = _w;}
    int32_t getV()       {  return v;  }
    int32_t getWeight()  {  return weight; }
};

// Class to represent a graph using adjacency list representation
class Graph
{
    int32_t V;    // No. of vertices'

    // Pointer to an array containing adjacency lists
    std::vector<std::list<AdjListNode>> adj;

    // A function used by longestPath
    void topologicalSortUtil(int32_t v, std::vector<bool> &visited, std::stack<int32_t> &Stack);
    std::list <int32_t> getMaxPath(std::vector<int32_t> dist, std::map<int32_t, int32_t> prev);
public:
    Graph(int32_t V);   // Constructor

    // function to add an edge to graph
    void addEdge(int32_t u, int32_t v);

    // Finds longest distances from given source vertex
    std::list<int32_t> longestPath(int32_t s);
    // .. from list<s>
    std::list<int32_t> longestPath(std::list<int32_t> fatherList);

};

#endif // GRAPH_H
