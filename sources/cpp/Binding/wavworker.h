#ifndef WAVWORKER_H
#define WAVWORKER_H

#include <string>
#include <cstring>
#include <iostream>


#if defined _MSC_VER

#elif defined __GNUC__
    #include <sndfile.h>
#endif
#ifndef Q_OS_WIN
#endif
#include <assert.h>
#include "../Utils/exceptionHandler.h"

const int64_t _16k = 16000;

// Нужен для чтения и разбивки файлов
// В данный моммент однопоточный
// одновременно 1 входной файл 1 выходной
class WavWorker
{
public:
    struct Settings
    {
        int samplesPerSecond; // Нужна для передескритизации
        int bitRate;
    };

    WavWorker();
    ~WavWorker();

    // Начинаем читать файл сначала
    // при этом закрываем старый, и открываем новый
    void openInFile(const std::string& fileName);
    void openOutFile(const std::string& fileName, const Settings&);

    // Вырезает часть из _in в _out
    void copyPart(int64_t pos, int64_t size);

    void setReadPos(int64_t pos);
    void setWritePos(int64_t pos);

    // Нужен для чтения следующей части файла, при разбивке
    // buffSize - сколько читать
    bool read(double* data, int64_t buffSize);
    // Записываем прочитанную инфу в открытый файл
    bool write(double* data, int64_t dataSize);

    int64_t sizeIn(); // TODO найти способ узнать длинну?
private:

#ifndef _MSC_VER
    SNDFILE	*_in;
    SNDFILE	*_out;

    void initFSInfo(SF_INFO&sfinfo, const Settings&);
    void defaultInitFSInfo(SF_INFO&sfinfo);
#endif
};

#endif // WAVWORKER_H
