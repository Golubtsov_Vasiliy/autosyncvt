#include "wavworker.h"

#ifndef _MSC_VER
WavWorker::WavWorker()
{
    _in = NULL;
    _out = NULL;
}

WavWorker::~WavWorker()
{

}

void WavWorker::setWritePos(int64_t pos)
{
    int64_t readed = sf_seek(_out, pos, SEEK_SET);
    assert(readed == pos);
}

void WavWorker::setReadPos(int64_t pos)
{
    int64_t readed = sf_seek(_in, pos, SEEK_SET);
    assert(readed == pos);
}

void WavWorker::copyPart(int64_t pos, int64_t size)
{
    double* buff = new double[size];
    setReadPos(pos);
    read(buff, size);
    write(buff, size);
    delete buff;
}

void WavWorker::defaultInitFSInfo(SF_INFO&sfinfo)
{
    memset(&sfinfo, 0, sizeof (sfinfo));
}

void WavWorker::initFSInfo(SF_INFO& sfinfo, const Settings& settings)
{
    defaultInitFSInfo(sfinfo);
    sfinfo.samplerate	= settings.samplesPerSecond;
    sfinfo.channels		= 1;
    switch (settings.bitRate) {
    case 16:
        sfinfo.format		= (SF_FORMAT_WAV | SF_FORMAT_PCM_16);
        break;
    default:
        ExceptionHandler::critical("Can no maintance bitRate: " + settings.bitRate);
        break;
    }
}

void WavWorker::openInFile(const std::string& fileName)
{
    SF_INFO	sfinfo;
    if (_in)
        sf_close(_in);
    defaultInitFSInfo(sfinfo);
    _in = sf_open(fileName.c_str(), SFM_READ, &sfinfo);
    if (!_in)
    {
        // ERROR
        exit(1);
    };
}

void WavWorker::openOutFile(const std::string& fileName, const Settings& settings)
{
    SF_INFO sfinfo;
    if (_out)
        sf_close(_out);
    initFSInfo(sfinfo, settings);
    _out = sf_open (fileName.c_str(), SFM_WRITE, &sfinfo);
    if (!_out)
    {
        ExceptionHandler::critical("Can not open output file: " + fileName);
        exit(1);
    };
}

bool WavWorker::read(double* data, int64_t buffSize)
{
    bool succes = sf_read_double(_in, data, buffSize);
    return succes;
}

bool WavWorker::write(double* data, int64_t dataSize)
{
    bool succes = sf_write_double (_out, data, dataSize);
    return succes;
}

int64_t WavWorker::sizeIn()
{
    int64_t sugestSize = 60 * _16k;
    double buffer[sugestSize];
    if (read(buffer, sugestSize))
        return sugestSize;
    return 0;
}
#else
WavWorker::WavWorker()
{

}

WavWorker::~WavWorker()
{

}

void WavWorker::setWritePos(int64_t pos)
{
    assert(false);
}

void WavWorker::setReadPos(int64_t pos)
{
    assert(false);
}

void WavWorker::copyPart(int64_t pos, int64_t size)
{
    assert(false);
}

void WavWorker::openInFile(const std::string& fileName)
{
    assert(false);
}

void WavWorker::openOutFile(const std::string& fileName, const Settings& settings)
{
    assert(false);
}

bool WavWorker::read(double* data, int64_t buffSize)
{
    assert(false);
    return false;
}

bool WavWorker::write(double* data, int64_t dataSize)
{
    assert(false);
    return false;
}

int64_t WavWorker::sizeIn()
{
    assert(false);
    return 0;
}
#endif
