#ifndef ASR_H
#define ASR_H

#include <string>
#include <cstdint>
#include <list>
#include <map>

#include "../Utils/dirworker.h"
#include "scripter.h"

// Интерфейс распознователя речи
class IASR
{
public:

    // Синхронные методы
    virtual void recognize(const std::list<std::string>& fileNames) = 0;
    virtual void recognize(const std::string& fileName) = 0;

    // <word> <time begin> <time end>
    virtual StringUtils::RecognizedString getRecognized(const std::string& fileName) const = 0;
};

// Класс для распознования речи в wav файлах, на Linux, при помощи Kaldi
// Ограничения на wav:
// 1) Частота дискретизации 16000 Гц
// 2) Битрейд: 16 бит
// 3) Должны оканчиваться на *.wav
// Желательно длительность <= 7 секунд.
class ASRScripter : public IASR
{
public:

    void recognize(const std::list<std::string>& fileNames) override;
    void recognize(const std::string& fileName)  override;
    StringUtils::RecognizedString getRecognized(const std::string& fileName) const override;

    // podobnoe moget vizvat oshibku pri chtenii
    //int32_t getRecognizedLength(const std::string& fileName) const override;

    ASRScripter();
private:
    // Путь до скрипта распознования
    const std::string SCRIPT_PATH = DirWorker::getCurrentPath() + StringUtils::curSplitDirString + "Scripts" + StringUtils::curSplitDirString;
    // Скрипт распознования
    const std::string RECOGNIZE_SCRIPT = SCRIPT_PATH + "wavToString.sh";
    std::string _file_name;
    std::map <std::string, StringUtils::RecognizedString> _fileSpeach;

    Scripter _scripter;

    StringUtils::RecognizedString handleRecognizedString(const std::wstring &recognized);
    StringUtils::RecognizedString handleRecognizedString_sphinx(const std::wstring& recognized);
    StringUtils::RecognizedString handleRecognizedString_kaldi(const std::wstring& recognized);
    void startRecognizeScript(const std::list<std::string> &args);
};

#endif // ASR_H
