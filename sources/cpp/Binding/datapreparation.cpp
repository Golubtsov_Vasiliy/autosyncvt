#include "datapreparation.h"

DataPreparation::DataPreparation()
{

}

std::string DataPreparation::prepeareWav(const std::string& videoFile)
{
    std::string wavFile = _scripter.extractAudio(videoFile);
    //generateNoise_wav(wavFile, _noise_wav);
    //generateNoise_wav(wavFile, _noise_wav, _noiseWindowBegin, _noiseWindowEnd);
    // В данной версии не используем метод определения шума по шаблону
    _scripter.noiseReduse(wavFile, _noise_wav);
   // _scripter.noiseReduse(wavFile, _noise_wav);
    _wav.openInFile(wavFile);
    int64_t sampleSize = _wav.sizeIn();
    double secondTime = sampleSize / _16k;
    //_scripter.createPreview(videoFile, secondTime);
    return wavFile;
}

void DataPreparation::generateNoise_wav(const std::string& fileName, const std::string& noiseWav, int64_t begin, int64_t end)
{
    std::string tmp_filtred = "f_" + fileName;
    if (FileWorker::exists(tmp_filtred))
        FileWorker::remove(tmp_filtred);
    FileWorker::copy(fileName, tmp_filtred);
    _scripter.noiseReduse(tmp_filtred, tmp_filtred);

    _wav.openInFile(tmp_filtred);
    _wav.setReadPos(begin);
    int64_t curPos = begin;
    int64_t globalMinPos = curPos;
    double globalMinEnergy;
    int64_t step = _noiseFileSize / 4;
    double* buff = new double[_noiseFileSize];
    _wav.read(buff, step * 3);
    bool f_processing = true;
    while (f_processing && curPos + step*5 < end)
    {
        double* readBuffer = new double[step];
        f_processing = _wav.read(readBuffer, step);
        double* tmp = buff;
        buff = join(buff, 3*step, readBuffer, step);
        delete tmp;
        delete readBuffer;
        double localEnergy = getEnergy(buff, 0, step * 4);
        if (curPos == begin || localEnergy < globalMinEnergy)
        {
            globalMinPos = curPos;
            globalMinEnergy = localEnergy;
        }
        tmp = buff;
        buff = splitBegin(buff, 4*step, step);
        delete tmp;
        curPos += step;
    }
    delete buff;

    _wav.openInFile(fileName);
    WavWorker::Settings settings;
    writePreparation(settings);
    if (FileWorker::exists(noiseWav))
        FileWorker::remove(noiseWav);
    _wav.openOutFile(noiseWav, settings);
    _wav.copyPart(globalMinPos, _noiseFileSize);
}

double* DataPreparation::join(double* prefix, int64_t prefixSize, double* data, int64_t dataSize) const
{
    double* rez = new double[prefixSize + dataSize];
    for (int64_t i = 0; i < prefixSize; i++)
        rez[i] = prefix[i];
    for (int64_t i = 0; i < dataSize; i++)
        rez[prefixSize + i] = data[i];
    return rez;
}

double* DataPreparation::splitBegin(double* source, int64_t sourceSize, int64_t splitSize) const
{
    double* rez = new double[sourceSize - splitSize];
    for (int64_t i = 0; i < sourceSize - splitSize; i++)
        rez[i] = source[splitSize + i];
    return rez;
}

double DataPreparation::getEnergy(double* data, int64_t begin, int64_t end)
{
    double energy = 0;
    for (int64_t i = begin; i < end; i++)
    {
        // это не мощность, на windSize делить не обязательно
        // так как окно всегда одинакого, деление не чего не даёт
        double localEnergy = data[i]; // Можно использовать окно хеминга для вычисления, но нас тут не итересует точность
        energy += localEnergy * localEnergy;
    }
    return energy;
}

int64_t DataPreparation::getEnergyMin(double* data, int64_t dataSize, int64_t windowSize)
{
    int64_t edgeOffset = windowSize/2;
    int64_t minPos = 0;
    double curMinLevel = -1;
    for (int64_t i = edgeOffset; i<dataSize-edgeOffset; i+=windowSize/4) // TODO можно попробовать обратный обход
    {
        int64_t beginWind = i-edgeOffset;
        int64_t endWind = i+edgeOffset;
        double curEnergyLevel = getEnergy(data, beginWind, endWind);
        if (curEnergyLevel < curMinLevel || curMinLevel < 0)
        {
            curMinLevel = curEnergyLevel;
            minPos = i;
        }
    }
    return minPos;
}

double DataPreparation::getStadyComponent(double* data, int64_t dataSize)
{
    double summ = 0;
    for (int64_t i = 0 ; i<dataSize; i++)
        summ+= data[i];
    double avg = summ/dataSize;
    return avg;
}

void DataPreparation::useMultyWindow(double* data, int64_t dataSize)
{
   // for (qint64 i = 0; i<dataSize; i++)
    //    data[i] *= 0.54f - 0.46f*cos(M_2_PI*i/(dataSize-1));
}

void DataPreparation::deleteStadyComponent(double* data, int64_t dataSize)
{
    double stadyComponent = getStadyComponent(data, dataSize);
    for (int64_t i = 0; i<dataSize; i++)
        data[i] -= stadyComponent;
}

void DataPreparation::writePreparation(WavWorker::Settings& settings, const std::string& outDirrectory)
{
    if (DirWorker::exists(outDirrectory) == false)
        DirWorker::mkDir(outDirrectory);
    // Щитаем шаг
    settings.samplesPerSecond = _16k;
    settings.bitRate = 16;
}

void DataPreparation::splitFile(const std::string& fileName, double minSplitSize, double maxSplitSize, const std::string& outDirrectory)
{
    assert(minSplitSize > 0 && maxSplitSize > 0 && maxSplitSize > minSplitSize);
    WavWorker::Settings settings;
    writePreparation(settings);
    std::string absoluteSplitDir = DirWorker::absolutePath(SPLIT_DIR);
    if (DirWorker::exists(absoluteSplitDir))
        DirWorker::clearDir(absoluteSplitDir);
    else
        DirWorker::mkDir(absoluteSplitDir);
    int64_t discret = settings.samplesPerSecond;
    int64_t maxStep = maxSplitSize * discret;
    int64_t minStep = minSplitSize * discret;
    double* buff = new double[2*maxStep];
    if (FileWorker::exists(fileName) == false)
    {
        //qDebug() << "Incorrect file name or file not exists: " << fileName << "\n";
        return;
    }
    int64_t partNumber = 0;
    int64_t curBuffSize = 0;
    bool f_processing = true;
    int64_t curPos = 0;
    _wav.openInFile(fileName);
    while (f_processing)
    {
        std::string ID = std::to_string(partNumber);
        std::string outputName = formNPurtName(fileName, ID);
        if (curBuffSize < maxStep)
        {
            int64_t readBufferSize = maxStep;
            double* readBuffer = new double[readBufferSize];
            f_processing = _wav.read(readBuffer, readBufferSize);
            double* tmp = buff;
            buff = join(buff, curBuffSize, readBuffer, readBufferSize);
            delete tmp;
            delete readBuffer;
            curBuffSize += maxStep;
        }
        assert(curBuffSize >= minStep);
        int64_t minPos = minStep + getEnergyMin(buff + minStep, curBuffSize - minStep);
        _wav.openOutFile(outputName, settings);
        _wav.write(buff, minPos);
        _rezFileNameList.push_back(outputName);
        _rezFileBeginOffset[outputName] = (floor(10 * (double)curPos / discret))/10;
        _rezFileEndOffset[outputName] = (floor(10 * (double)(curPos + minPos) / discret))/10;
        curPos += minPos;
        double* tmp = buff;
        buff = splitBegin(buff, curBuffSize, minPos);
        curBuffSize -= minPos;
        delete tmp;
        partNumber++;
    }
    delete buff;
    for (auto part : _rezFileNameList)
        _scripter.normalise(part);
}

void DataPreparation::splitFile(const std::string &fileName, double splitSize, const std::string &outDirrectory)
{
    std::list<std::string> rezFileList;
    // Настраиваем файловую систему
    //if (fileIsCorrect(fileName) == false)
    //    return rezFileList;
    WavWorker::Settings settings;
    // Щитаем шаг
    writePreparation(settings);
    int64_t step = splitSize * settings.samplesPerSecond;

    //if (splitSize<1.0f)
        //qCritical() << "Soo shorty file split is don't good idea";

    double *buff = new double[step];
    _wav.openInFile(fileName);
    int64_t partNumber = 0;
    int64_t curPos = 0;
    while (_wav.read(buff, step))
    {
        // Note: так как буфер не пересчитываеться поседний файл записываеться с тишиной
        std::string ID = std::to_string(partNumber);
        std::string outputName = formNPurtName(fileName, ID);
        _wav.openOutFile(outputName, settings);
        _wav.write(buff, step);
        rezFileList.push_back(outputName);
        partNumber++;
        _rezFileBeginOffset[outputName] = curPos;
        _rezFileEndOffset[outputName] = curPos + step;
        curPos += step;
    }
    delete buff;
}

// Нужна для разбиения файла на части
std::string DataPreparation::formNPurtName(const std::string& sourceName, const std::string& ID)
{
    std::string shortName = FileWorker::baseName(sourceName);
    std::string dir = DirWorker::absolutePath(SPLIT_DIR);
    std::string rezName = dir + StringUtils::curSplitDirString + ID + "_" + shortName + ".wav";
    return rezName;
}

void DataPreparation::clearDefaultSplitDir()
{
    clearDir(SPLIT_DIR);
    _rezFileNameList.clear();
    _rezFileBeginOffset.clear();
    _rezFileEndOffset.clear();

}

void DataPreparation::clearTrash()
{
    auto thrshList = DirWorker::entryInfoList("*.wav");
    for (auto trash : thrshList)
    {
        std::string trashFIle = trash;
        FileWorker::remove(trashFIle);
    }
}

bool DataPreparation::copy(const std::string& originalDir, const std::string& copyDir, const std::list<std::string>& mask)
{
    bool rez = true;
    auto originalFiles = DirWorker::entryInfoList(originalDir, mask);
    if (originalFiles.size() == 0)
        return false;
    for (auto file : originalFiles)
    {
        std::string base = FileWorker::fileName(file);
        std::string copyName = copyDir + StringUtils::curSplitDirString + base;
        if (FileWorker::copy(file, copyName) == false)
            rez = false;
    }
    return rez;
}

bool DataPreparation::clearDir(const std::string &dir)
{
    DirWorker::clearDir(dir);
    return true;
    /*const QFileInfoList fileList = dir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot);

    bool result = true;
    QFile file;

    auto cur = fileList.constBegin();
    auto end = fileList.constEnd();
    for (; result && cur != end; cur++)
    {
        const QString fileName = cur->absoluteFilePath();

        qDebug () << QString("Remove ") << fileName;

        result = QFile::remove(fileName);

        if (!result)
            qDebug () << file.errorString ();
    }*/

    //return result;
}
