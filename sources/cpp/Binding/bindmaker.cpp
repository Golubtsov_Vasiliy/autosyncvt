#include "bindmaker.h"

BindMaker::BindMaker(TextStore::PTR textStore, SoundStore::PTR soundStore, Logic::PTR logic)
{
     _asr = new ASRScripter();
    _textStore = textStore;
    _soundStore = soundStore;
    _logic = logic;
    _splitSize = 4.0f;
    _diff = 2.0f;
    _maxWordError = 3;
    _maxWrongPersent = 0.63;
    //_curState = _baseState.arg(0, 0, 0);
    clear();
}

void BindMaker::clear()
{
    _preparator.clearDefaultSplitDir();
    //_asr->; там мало инфы но возможно тоже стоит чистить
    _fileBeginOffset.clear();
    _fileEndOffset.clear();
    _recognizedStrings.clear();
    _processList.clear();
    _fileParts.clear();
    _localMin.clear();
    _textLength.clear();
}

void BindMaker::preparetion()
{
    //qDebug() << "******* Preparetion *******";
    //_curState = _baseState.arg(0).arg(0).arg(0);
    clear();
    //_curState = _baseState.arg(10).arg(0).arg(0);
    if (_logic->haveRecognizedString())
    {
        _logic->clear(false);
        soundPreparetion();
    }
    else
    {
        _logic->clear(true); // Если занимались деление файла - старые распознаные данные считаем устаревшими
        soundPreparetion(_splitSize, _diff);
    }

    //_curState = _baseState.arg(70).arg(0).arg(0);
    sortFileParts();

    for (auto name : _fileParts)
        _processList[name] = false;

    _textStore->prepeareWordSplit();
    //_curState = _baseState.arg(100).arg(0).arg(0);
}

void BindMaker::soundPreparetion()
{
    _recognizedStrings = _logic->getRecognizedStrings();

    _fileBeginOffset = _logic->getRecognizedStringBeginList();
    _fileEndOffset = _logic->getRecognizedStringEndList();
    for (auto& el : _recognizedStrings)
        _fileParts.push_back(el.first);
}

void BindMaker::sortFileParts()
{
    std::map <int32_t, std::string> mids;
    std::vector <int32_t> midKeys;
    midKeys.reserve(_fileParts.size());
    for (auto fileName: _fileParts)
    {
        double doubleMid = getFileMid(fileName);
        int32_t intMid = doubleMid*100;
        mids[intMid] = fileName;
        midKeys.push_back(intMid);
    }
    std::sort(midKeys.begin(), midKeys.end());
    _fileParts.clear();
    for (auto midKey : midKeys)
    {
        std::string nextString = mids[midKey];
        if (std::find(_fileParts.begin(), _fileParts.end(), nextString) == _fileParts.end()) // Защита от глюка с "очень близкими" словами
            _fileParts.push_back(nextString);
    }
}

void BindMaker::soundPreparetion(double splitSize, double diff)
{
    assert(splitSize - diff > 0);
    std::string videoFile = _soundStore->fileUrl();
    std::string wavFile = _preparator.prepeareWav(videoFile);
    //_curState = _baseState.arg(40).arg(0).arg(0);
    if (diff == 0.0f)
        _preparator.splitFile(wavFile, splitSize);
    else
    {
        double max = splitSize + diff;
        double min = splitSize - diff;
        _preparator.splitFile(wavFile, min, max);
    }
    _fileParts = _preparator.getFileNameList();
    _fileBeginOffset = _preparator.getFileBeginList();
    _fileEndOffset = _preparator.getFileEndList();
}

void BindMaker::recognizing()
{
    if (_recognizedStrings.empty())
    {
        //qDebug() << "******* Recognizing *******";
        int32_t handled = 0;
        for (auto file : _fileParts)
        {
            _asr->recognize(file);
            auto recognized = _asr->getRecognized(file);
            double begin = _fileBeginOffset[file];
            double end = _fileEndOffset[file];
            StringUtils::offset(recognized, begin);
            _recognizedStrings[file] = recognized;

            // fix to use kaldi without word time
            if (recognized.empty() == false)
            {
                _recognizedStrings[file].front()._posBegin = begin;
                _recognizedStrings[file].back()._posEnd = end;
            }

            _logic->addRecognizedString(recognized, begin, end, file);
            handled++;
            //double processPersent = 100 * (qreal)handled/_fileParts.size();
            //_curState = _baseState.arg(100).arg(processPersent).arg(0);
        }
    }
}

void BindMaker::binding()
{
    //qDebug() << "******* Calculate local mins *******";
    int32_t handled = 0;
    for (auto nextPart : _fileParts)
    {
        //qDebug() << "\nHandle file: " << nextPart;
        _processList[nextPart] = true;
        calcLocalMin(nextPart, _maxWrongPersent);
        handled++;
        //double processPersent = 50*(qreal)handled/_fileParts.size();
        //_curState = _baseState.arg(100).arg(100).arg(processPersent);
    }
    //qDebug() << "******* Binding *******";
    useLocalMinToFind_bind();
    //_curState = _baseState.arg(100).arg(100).arg(100);
    //qDebug() << "******* Complete *******";
}

void BindMaker::run()
{
    preparetion();
    if (recognizeIsFinished())
    {
        //qDebug() << "U have not prepeared sound data" << "\n";
        return;
    }
    recognizing();
    if (_textStore->empty())
    {
        //qDebug() << "U have not text data" << "\n";
        return;
    }
    binding();
    postWordHanding();
}

void BindMaker::fillInGraph(Graph &g)
{
    int32_t curID = 0;
    for (auto curFile = _fileParts.begin(); curFile != _fileParts.end(); curFile++)
        for (auto localMin : _localMin[*curFile])
        {
            _localMinToID[*curFile][localMin] = curID;
            _IDToFileName[curID] = *curFile;
            _IDToLoacalMin[curID] = localMin;
            curID++;
        }

    for (auto curFile = _fileParts.begin(); curFile != std::prev(_fileParts.end()); curFile++)
        for (int32_t i = 1; std::next(curFile, i) !=_fileParts.end(); i++)
           // for (auto localMin : _localMin[*curFile])
            {
                if (_localMin[*curFile].empty())
                    continue;
                auto localMin = _localMin[*curFile].front();
                auto nextFile = std::next(curFile, i);
                int32_t beginID = _localMinToID[*curFile][localMin];
                int32_t endID = getLocalMinEnd(*curFile, localMin, *nextFile);
                assert(beginID < curID && endID < curID);
                assert(*curFile != *nextFile);
                assert(beginID != endID);
                if (endID != -1 && beginID != -1)
                    g.addEdge(beginID, endID);
            }
}

// Локальный минимум конца, если данный локальный минимум соответствует реальной позиции в тексте
int32_t BindMaker::getLocalMinEnd(const std::string& fileName, int32_t localMinBegin, const std::string& nextFileName) const
{
    auto recognizedString = _recognizedStrings.at(fileName);
    int32_t recSize = StringUtils::getRecognizedLength(recognizedString);
    int32_t minOffset = recSize * 2 / 3;

    int32_t rezLocalMin=-1;
    for (auto localMin : _localMin.at(nextFileName))
        if (localMin > localMinBegin + minOffset)
        {
            rezLocalMin = localMin;
            break;
        }

    if (rezLocalMin == -1)
        return -1;
    int32_t rezID = _localMinToID.at(nextFileName).at(rezLocalMin);
    return rezID;
}

void BindMaker::addBinsFromList(const std::list<int32_t> &localMinsIDlist) const
{
    for (auto curID = localMinsIDlist.begin(); std::next(curID) != localMinsIDlist.end(); curID++)
    {
        std::string fileStringBegin = _IDToFileName.at(*curID);
        std::string fileStringEnd = _IDToFileName.at(*(std::next(curID)));
        double beginSound = _fileBeginOffset.at(fileStringBegin);
        double endSound = _fileBeginOffset.at(fileStringEnd); // Начало следующего считаем концом предыдущего
        auto soundFragment = SoundFragment::factoryMethod(beginSound, endSound, _soundStore);

        int32_t beginText = _IDToLoacalMin.at(*curID);
        int32_t endText = _IDToLoacalMin.at(*(std::next(curID)));
        auto textFragment = TextFragment::factoryMethod(beginText, endText, _textStore);

        _logic->makeBind(textFragment, soundFragment);
        auto rec = _logic->getRecognizedString(fileStringBegin);
        // TODO их можетт быть несколько -> стоит искать от начала к концу бинда
        //int32_t curBindNumber = _logic->getBindVectorSize() - 1;
        _logic->addRecognizedInBind(rec);
    }
}

int32_t BindMaker::getLocalMinNumber() const
{
    int32_t rezNumber = 0;
    for (std::string file : _fileParts)
        if (_localMin.find(file) != _localMin.end())
            rezNumber += _localMin.at(file).size();
    return rezNumber;
}

//void BindMaker::processLongestPath(double curPersent)
//{
//    _curState = _baseState.arg(100).arg(100).arg(50 + curPersent * 50);
//    emit process();
//}

double BindMaker::getMid(double begin, double end) const
{
    const double eps = 0.01;
    double mid = begin + eps; // Из за ошибок системы распознования возможно, что мид более правого (с более правым началом) окажется левее мида левого
    return mid;
}

double BindMaker::getFileMid(const std::string& fileName) const
{
    double begin = _fileBeginOffset.at(fileName);
    double end = _fileBeginOffset.at(fileName);
    double mid = getMid(begin, end);
    return mid;
}

std::string BindMaker::getRespectRecFile(double mid) const
{
    for (std::string fileName : _fileParts)
    {
        double fileBegin = _fileBeginOffset.at(fileName);
        double fileEnd = _fileEndOffset.at(fileName);
        if (mid > fileBegin && mid > fileEnd)
            return fileName;
    }
    ExceptionHandler::critical("Not found file with momment (mid) " + std::to_string(mid));
    return "ERROR";
}

int32_t BindMaker::getRealWordPos(const Logic::Bind& bind, int32_t offsetPos)
{
    TextFragment::PTR text = bind.text;
    int32_t beginOffset = text->begin();
    if (offsetPos == 0)
        return beginOffset;
    std::wstring bindString = text->getString();
    std::list<std::wstring> wordSplit = StringUtils::split(bindString, ' ');
    std::list<std::wstring> subStrOffset;
    int32_t begin = 0;
    int32_t end = wordSplit.size() - 1;
    if (offsetPos>0)
        subStrOffset = StringUtils::getSubList(wordSplit, begin, begin + offsetPos - 1);
    else
        subStrOffset = StringUtils::getSubList(wordSplit, begin, end + offsetPos);
    int32_t rezOffset = StringUtils::lengthBeforeSplit(subStrOffset);
    int32_t realPos = beginOffset;
    if (offsetPos>0)
        realPos += rezOffset;
    else
        realPos -= rezOffset;
    return realPos;
}

//void BindMaker::calcCandidates()
//{
//    std::vector<Logic::Bind> allBinds = _logic->getBindVector();
//    auto beginBind = allBinds.begin();
//    auto endBind = allBinds.end();
//    for (auto bind = beginBind; bind != endBind; bind++)
//    {
//        int32_t beginOffset = -1000;
//        std::vector<std::wstring> wordCandidates = _logic->getWordCandidatesFromPosition(bind, beginBind, endBind, _maxWordError, beginOffset);
//        SoundFragment::PTR sound = bind->sound;
//        double bindBegin = sound->begin();
//        double bindEnd = sound->begin();
//        double bindMid = getMid(bindBegin, bindEnd);
//        std::string respectString = getRespectRecFile(bindMid);
//        _correctCandidates[respectString] = wordCandidates;
//        // Ищем позиции слов
//        for (int32_t i = 0; i<wordCandidates.size(); i++)
//        {
//            std::vector<Logic::Bind>::iterator candBind = bind;
//            int32_t realOffset = i + beginOffset;
//            if (realOffset < 0)
//                candBind = std::prev(bind);
//            int32_t realPos = getRealWordPos(candBind, realOffset);
//            _fileCandidatePos[respectString][i] = ;
//        }
//    }
//}

//void BindMaker::makeSplit(splitVector& splits, const std::string& fileName, int32_t beginWord, int32_t endWord, double beginSound, double endSound)
//{
//    if (beginWord - endWord <= 1)
//        return;

//    StringUtils::RecognizedString rC = _recognizedStrings.at(fileName); // лучше вынести на верх рекурсии
//    std::vector<std::wstring> wC = _correctCandidates.at(fileName); // это тоже

//    StringUtils::RecognizedString cur_rC = StringUtils::getSubRecognizedString(rC, beginSound, endSound);
//    std::vector<std::wstring> cur_wC = StringUtils::getSubVector(wC, beginWord, endWord);

//    // Ищем ключевое слово
//    for (auto recWord = cur_rC.begin(); recWord != cur_rC.end(); recWord++)
//    {
//        std::wstring wordStr = recWord->_recognizedString;
//        if (std::count(cur_wC.begin(), cur_wC.end(), wordStr) == 1)
//        {
//            auto keyI = std::find(cur_wC.begin(), cur_wC.end(), wordStr);
//            int32_t vectorWordPos = std::distance(cur_wC.begin(), keyI);
//            //int32_t vectorRecPos = std::distance(cur_rC.begin(), recWord);
//            Spliter split;
//            double timeSplit = recWord->_posBegin;
//            double timeSplitEnd = recWord->_posEnd;
//            int32_t wordSplit = beginWord + vectorWordPos;
//            split._soundSplit_timeSplit = timeSplit;
//            split._candSplit = wordSplit;

//            makeSplit(splits, fileName, beginWord, wordSplit-1, beginSound, timeSplit);
//            makeSplit(splits, fileName, wordSplit+1, endWord, timeSplitEnd, endSound);

//            break;
//        }
//    }
//}

void BindMaker::addCandidatesFromBind(std::vector<std::wstring>& candidates, const Logic::Bind& bind, int32_t wordsNumber) const
{
    TextFragment::PTR text = bind.text;
    std::vector<std::wstring> textWordString = text->getWordList();
    std::vector<std::wstring> words = StringUtils::getSubVector(textWordString, wordsNumber);
    for (std::wstring word : words)
        candidates.push_back(word);
}

std::vector<std::wstring> BindMaker::getCandidates(int32_t bindNumber) const
{
    std::vector<std::wstring> candidates;
    auto addFalseCandidates = [this](std::vector<std::wstring>& candidates){
        for (int32_t i = 0; i<_maxWordError; i++)
            candidates.push_back(L"--its_mean_nothing--"); // для простоты кода добавляем фальшивых кандидатов
    };
    if (bindNumber == 0)
        addFalseCandidates(candidates);
    else
    {
        Logic::Bind prevBind = _logic->getBind(bindNumber-1);
        addCandidatesFromBind(candidates, prevBind, -_maxWordError);
    }

    Logic::Bind bind = _logic->getBind(bindNumber);
    TextFragment::PTR text = bind.text;
    std::vector<std::wstring> bindCandidates = text->getWordList();
    for (int32_t i = 0; i<bindCandidates.size(); i++)
    {
        std::wstring word = bindCandidates.at(i);
        candidates.push_back(word);
    }

    if (bindNumber>=_logic->getBindVectorSize()-1)
        addFalseCandidates(candidates);
    else
    {
        Logic::Bind nextBind = _logic->getBind(bindNumber+1);
        addCandidatesFromBind(candidates, nextBind, _maxWordError);
    }
    return candidates;
}

// Ищет уникальное слово Spliter
void BindMaker::findUniq(const std::vector<std::wstring>& candidates, const std::vector<StringUtils::RecognizedWord>& recognized, int32_t prevC, int32_t nextC, int32_t prevR, int32_t nextR, int32_t& uniqC, int32_t& uniqR) const
{
    auto rBegin = std::next(recognized.begin(), prevR);
    auto rEnd = std::next(recognized.begin(), nextR+1);
    auto cBegin = std::next(candidates.begin(), prevC);
    auto cEnd = std::next(candidates.begin(), nextC+1);

//    std::wstring recWord = L"ten";
//    int32_t test = std::count_if(rBegin, rEnd, [recWord](const StringUtils::RecognizedWord& rWord){
//        return StringUtils::isEquils(rWord, recWord);
//    });

    for (int32_t curR = prevR; curR <= nextR; curR++)
    {
        StringUtils::RecognizedWord rWord = recognized.at(curR);
        std::wstring recWord = rWord._recognizedString;

        int32_t simularRec = std::count_if(rBegin, rEnd, [recWord](const StringUtils::RecognizedWord& rWord){
            return StringUtils::isEquils(rWord, recWord);
        });
        if (simularRec != 1)
            continue;
        int32_t simularCand = std::count(cBegin, cEnd, recWord);
        if (simularCand != 1)
            continue;

        std::vector<StringUtils::RecognizedWord>::const_iterator i_unicR = std::find_if(rBegin, rEnd, [recWord](const StringUtils::RecognizedWord& rWord){
            return StringUtils::isEquils(rWord, recWord);
        });
        std::vector<std::wstring>::const_iterator i_unicC = std::find(cBegin, cEnd, recWord);

        uniqC = std::distance(cBegin, i_unicC) + prevC;
        uniqR = std::distance(rBegin, i_unicR) + prevR;
        assert(uniqC>=0);
        assert(uniqR>=0);
        return;
    }
}

void BindMaker::makePostSplit(std::vector<Spliter>& rez, const std::vector<std::wstring>& candidates, const std::vector<StringUtils::RecognizedWord>& recognized, int32_t prevC, int32_t nextC, int32_t prevR, int32_t nextR, const std::map<int32_t, int32_t>& candTextPos) const
{
    int32_t uniqC = -1;
    int32_t uniqR = -1;
    findUniq(candidates, recognized, prevC, nextC, prevR, nextR, uniqC, uniqR);
    if (uniqC < 0/* || uniqR < 0*/)
        return;

    Spliter newLeftSplit;
    Spliter newRightSplit;
    StringUtils::RecognizedWord splitWord = recognized.at(uniqR);
    newLeftSplit._soundSplit = splitWord._posBegin;
    newLeftSplit._textSplit = candTextPos.at(uniqC);
    newRightSplit._soundSplit = splitWord._posEnd;
    int32_t candL = splitWord._recognizedString.size();
    newRightSplit._textSplit = newLeftSplit._textSplit + candL;
    rez.push_back(newLeftSplit);
    rez.push_back(newRightSplit);

    if (uniqC-1 >= prevC && uniqR-1 >= prevR) // выход из рекурсии
        makePostSplit(rez, candidates, recognized, prevC, uniqC-1, prevR, uniqR-1, candTextPos); // .. Вход
    if (uniqC+1 <= nextC && uniqR+1 <= nextR) // выход из рекурсии
        makePostSplit(rez, candidates, recognized, uniqC+1, nextC, uniqR+1, nextR, candTextPos); // .. Вход
}

std::map<int32_t, int32_t> BindMaker::getCandidatesPos(int32_t firstPos, const std::vector<std::wstring>& candidates) const
{
    int32_t firstCandidateWordPos = _textStore->wordNumberOnPos(firstPos);

    std::map<int32_t, int32_t> candidatesWordPos;
    int32_t curPos = firstPos;
    for (int32_t i_candidate = _maxWordError; i_candidate < candidates.size(); i_candidate++)
    {
        int32_t curWordPos = firstCandidateWordPos + i_candidate - _maxWordError;
        candidatesWordPos[i_candidate] = _textStore->wordRealPos(curWordPos);
        std::wstring curCandidate = candidates.at(i_candidate);
        int32_t candidateL = curCandidate.length();
        curPos+= candidateL;
    }
    return candidatesWordPos;
}

void BindMaker::handleBindsUsingSplits(const SplitVector& spliters)
{
    double eps = 0.01;
    int32_t bindsNumber = _logic->getBindVectorSize();
    int32_t splitsNumber = spliters.size();
    if (bindsNumber * 2 > splitsNumber)
        ExceptionHandler::warning("Final spliter vector is to small");

    _logic->clear(false);

    int32_t firstTextPos = 0;
    double firstSoundPos = 0.0f;
    for (Spliter split : spliters)
    {
        int32_t splitTextPos = split._textSplit;
        double splitSoundPos = split._soundSplit;

        if (splitTextPos - firstTextPos < eps)
            continue;
        if (splitSoundPos - firstSoundPos < eps)
            continue;
        TextFragment::PTR newBindText = TextFragment::factoryMethod(firstTextPos, splitTextPos, nullptr);
        SoundFragment::PTR newBindSound = SoundFragment::factoryMethod(firstSoundPos, splitSoundPos, nullptr);

        _logic->makeBind(newBindText, newBindSound);
        firstTextPos = splitTextPos;
        firstSoundPos = splitSoundPos;
    }
}

void BindMaker::sortSplitVector(SplitVector &spliters) const
{
    const double eps = 0.0001;
    std::sort(spliters.begin(), spliters.end(), [eps](const Spliter& left, const Spliter& right){
        if (abs(left._soundSplit - right._soundSplit) < eps)
            if (left._textSplit < right._textSplit)
                return true;
            else
                return false;

        if (left._soundSplit < right._soundSplit)
            return true;
        return false;
    });
}

void BindMaker::destroyColisions(SplitVector &splits) const
{
    for (auto lSplit = splits.begin(); lSplit != splits.end();)
    {
        int32_t minTextPos = (*lSplit)._textSplit;
        // Ищем колизии
        bool haveColisions = false;
        int32_t colisionNumber = 0;
        while (true)
        {
            auto colision = std::find_if(std::next(lSplit), splits.end(), [minTextPos](const Spliter& rSplit){
                if (rSplit._textSplit < minTextPos)
                    return true;
                return false;
            });
            if (colision == splits.end())
                break;
            haveColisions = true;
            splits.erase(colision);
            colisionNumber++;
        }
        if (!haveColisions)
        {
            lSplit++;
            continue;
        }
        //ExceptionHandler::warning("Finded split colision consist on " + std::to_string(colisionNumber) + " elements");
        auto nextSplit = std::next(lSplit);
        //splits.erase(lSplit);
        lSplit = nextSplit;
    }
}

void BindMaker::uniteClosest(SplitVector &splits) const
{
    double eps = 0.000001;
    SplitVector rezVector;
    rezVector.reserve(splits.size());
    for (int32_t i = splits.size() - 1;;)
    {
        if (i <= 0)
            break;
        double firstSound = splits.at(i-1)._soundSplit;
        double secondSound = splits.at(i)._soundSplit;
        if (abs(secondSound - firstSound) > eps) // secondSound != firstSound
        {
            Spliter i_split = splits.at(i);
            rezVector.push_back(i_split);
        }
        i--;
    }
    rezVector.push_back(splits.front()); // Так как его не проверяли
    for (int32_t i = 0; i< rezVector.size(); i++)
        splits[i] = rezVector[rezVector.size()-1-i];
}

void BindMaker::postWordHanding()
{
    auto bindVector = _logic->getBindVector();
    SplitVector splitVector;
    int32_t maxSplitNumber = _textStore->wordListSize() * 2;
    splitVector.reserve(maxSplitNumber);


    // TODO вместо всего что ниже искать максимально близкий к серединам и достаточно одного
    for (int32_t curBind = 0; curBind< bindVector.size(); curBind++)
    {
        std::vector <std::wstring> candidates;
        candidates = getCandidates(curBind);
        StringUtils::RecognizedString recognized;
        recognized = _logic->recognizedInBind(curBind);
        std::vector<StringUtils::RecognizedWord> recognizedVector = StringUtils::toVector(recognized);
        int32_t candidateNumber = candidates.size();
        int32_t recognizedNumber = recognizedVector.size();
        int32_t bindTextPos = _logic->getBindTextPos(curBind);
        auto candidatesWordPos = getCandidatesPos(bindTextPos, candidates);
        makePostSplit(splitVector, candidates, recognizedVector, 0, candidateNumber-1, 0, recognizedNumber-1, candidatesWordPos); // Тут надо искать максимально близций к середине
    }
// Заменить на оптимизацию по графу
    sortSplitVector(splitVector);
    destroyColisions(splitVector);
    uniteClosest(splitVector);
    handleBindsUsingSplits(splitVector);
}

//    splitVector splits;
//    calcCandidates();
//    splits.reserve(_fileParts.size() * 5);
//    for (std::string fileName : _fileParts)
//    {
//        splitVector fileSplits;
//        fileSplits.reserve(200);
//        makeSplit(fileSplits, fileName, 0, -1, 0, -1);
//        for (Spliter& s : file)
//        {
//            int32_t wordPos = _fileCandidatePos[fileName][s._candSplit];
//            s._wordSplit = wordPos;
//        }
//    }

//    std::sort(splits.begin(), splits.end(), [this](const Spliter& left, const Spliter& right){
//        return left._timeSplit < right._timeSplit;
//    });

//}

void BindMaker::useLocalMinToFind_bind()
{
    int32_t localMinNumber = getLocalMinNumber();
    Graph g(localMinNumber+1);
    fillInGraph(g);
    std::list <int32_t> fatherList; // Какие вершины могут быть первыми
    for (auto file : _fileParts)
    {
        if (_localMin[file].empty())
            continue;
        int32_t firstLocalMin = _localMin[file].front();
        int32_t firstLocalMinId = _localMinToID[file][firstLocalMin];
        fatherList.push_back(firstLocalMinId);
    }

    auto IDPath = g.longestPath(fatherList);
    addBinsFromList(IDPath);
}

std::list <int32_t> BindMaker::getCorelationFunc(const std::list<std::wstring> &recognizedString) const
{
    assert(_textStore->wordListSize() > recognizedString.size());
    std::list <int32_t> korelationFunc;
    if (recognizedString.size() == 0)
        return korelationFunc;
    std::list<std::wstring> curPartSourceText;
    int32_t i_curWord = 0;
    std::wstring curWord = _textStore->word(i_curWord);
    // Чтобы решить проблему конечности в начале добавляем бесмысленный текст
    for (int32_t i = 0; i<recognizedString.size() - 1; i++)
        curPartSourceText.push_back(L"-its_mean_nothing-");
    curPartSourceText.push_back(curWord);
    i_curWord++;

    auto folowedByLastWord = i_curWord;
    // считаем значения функции кореляции
    korelationFunc.push_back(recognizedString.size() + 1);
    for (int32_t i = -curPartSourceText.size()+1;; i++)
    {
        int32_t k = Metrics::getLevenshteinDistance(recognizedString, curPartSourceText);
        if (i <= 0)
            *(korelationFunc.begin()) = min(korelationFunc.front(), k);
        else
            korelationFunc.push_back(k);
        if (folowedByLastWord == _textStore->wordListSize())
            break;
        // Сдвигаем кореляционное окно
        std::wstring nextWord = _textStore->word(folowedByLastWord);
        curPartSourceText.push_back(nextWord);
        folowedByLastWord++;
        curPartSourceText.pop_front();
    }
    return korelationFunc;
}

void BindMaker::calcLocalMin(const std::string &partFileName, double maxWrongPersent)
{
    assert(maxWrongPersent >= 0 && maxWrongPersent < 1.0f); // Иначе не имеет смысла
    auto recognizedString = _recognizedStrings[partFileName];
    auto recognizedStringWordList = StringUtils::toStringList(recognizedString);
    std::list <int32_t> korelationFunc = getCorelationFunc(recognizedStringWordList);
    if (korelationFunc.empty())
        return;
    int32_t realMaxWrongValue = recognizedStringWordList.size();
    int32_t maxWrongValue = realMaxWrongValue * maxWrongPersent;
    // Вычисляем инфинум
    auto lastEl = korelationFunc.begin();
    auto curEl = korelationFunc.begin();
    auto nextEl = std::next(curEl);
    assert(nextEl != korelationFunc.end());
    int32_t i_curWord = 0;
    for (int32_t i=0; i<korelationFunc.size(); i++)
    {
        int32_t lastValue;
        int32_t nextValue;
        int32_t curValue = *curEl;
        lastValue = *lastEl;
        if (nextEl != korelationFunc.end())
            nextValue = *nextEl;
        else
            nextValue = korelationFunc.back();
        if (lastValue > maxWrongValue)
            lastValue = maxWrongValue;
        if (nextValue > maxWrongValue)
            nextValue = maxWrongValue;

        // Условие локального минимума
        if ((lastValue > curValue && nextValue >= curValue)
         || (lastValue >= curValue && nextValue > curValue))
        {
            int32_t realTextPos = _textStore->wordRealPos(i_curWord);
            _localMin[partFileName].push_back(realTextPos);
        }

        lastEl = curEl;
        curEl++;
        nextEl++;
        i_curWord++;
    }
    _textLength[partFileName] = StringUtils::getRecognizedLength(recognizedString);

    /*
    if (_localMin[partFileName].size() > 1)
    {
        int32_t firstPos = _localMin[partFileName].front();
        int32_t lastPos = _localMin[partFileName].back();
        qDebug() << "\nFind two local minimum for file: " << partFileName << " \n    on word positions:";
        for (auto pos : _localMin[partFileName])
            qDebug() << " " << pos;
        qint32 diff = lastPos - firstPos;
        qDebug() << "\n    Max difference: "  << diff;
        if (diff > recognizedString.size())
            qDebug() << "\n    WARNING!!! It have two infinum"
            << "\nFirts local minimum have " << firstPos << "/" << realMaxWrongValue
            << "\nLast local minimum have " << lastPos << "/" << realMaxWrongValue;
    } else if (_localMin[partFileName].size() == 0)
        qDebug() << "Don\'t find korelation for file: " << partFileName
                 << "\n Recognized string is :\"" << recognizedString << "\"";
    else if (recognizedString.length() > 0)
        qDebug() << "Ok. " << partFileName << " recognized";
    else
        qDebug() << "Something wrong.";*/
    return;
}

double BindMaker::calcProgress() const
{
    double progress = 1.0f;
    int32_t successPart = 0;
    int32_t full = _processList.size();
    for (auto isRecognized : _processList)
        if (isRecognized.second == true)
            successPart++;
    progress = (double)successPart/(double)full;
    return progress;
}

bool BindMaker::recognizeIsFinished()
{
    for (auto isRecognized : _processList)
        if (isRecognized.second == false)
            return false;
    return true;
}

int32_t BindMaker::getDistance(const std::list<std::wstring>& firstSeqence, const std::list<std::wstring>& secondSeqence) const
{
    return Metrics::getLevenshteinDistance(firstSeqence, secondSeqence);
}

//void BindMaker::deleteNotAlpha(std::wstring& str)
//{
//    for (int32_t i=0; i<str.length();)
//    {
//        wchar_t curSimbol = str[i];
//        if (std::iswalpha(curSimbol)==false /*&& curSimbol != '\''*/)
//            str = str.erase(i, 1);
//        else
//            i++;
//    }
//}
