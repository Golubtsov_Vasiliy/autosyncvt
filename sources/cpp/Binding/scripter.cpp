#include "scripter.h"

const std::string Scripter::_RecognizeSpeachScrit = DirWorker::getScriptPath() + "recognizeSpeach.sh";
const std::string Scripter::_RecognizeSpeachScrit_sphinx_ru = DirWorker::getScriptPath() + "recognizeSpeach_sphinx_ru.sh";
const std::string Scripter::_RecognizeSpeachScrit_sphinx_en = DirWorker::getScriptPath() + "recognizeSpeach_sphinx_en.sh";
const std::string Scripter::_RecognizeSpeachScrit_kaldi_en = DirWorker::getScriptPath() + "recognizeSpeach_kaldi_en.sh";
const std::string Scripter::_noiseReduseScrit = DirWorker::getScriptPath() + "noiseReduse.sh";
const std::string Scripter::_normaliseScrit = DirWorker::getScriptPath() + "normalise.sh";
const std::string Scripter::_extractSoundScrit = DirWorker::getScriptPath() + "extractSound.sh";
const std::string Scripter::_durationSoundScrit = DirWorker::getScriptPath() + "duration.sh";
const std::string Scripter::_downloadBaseScript = DirWorker::getScriptPath() + "downloadBase.sh";
const std::string Scripter::_initBaseScript = DirWorker::getScriptPath() + "initBase.sh";
const std::string Scripter::_uploadBaseScript = DirWorker::getScriptPath() + "uploadBase.sh";
const std::string Scripter::_createPreviewScript = DirWorker::getScriptPath() + "createPreview.sh";
const std::string Scripter::_chanchDiscretScript = DirWorker::getScriptPath() + "createPreview.sh";
const std::string Scripter::_tmp_wav = "/tmp.wav";

const std::string Scripter::_baseDir = "/eduEnReaderBase";

Scripter::Scripter()
{
    //_console = new QProcess();
    //QObject::connect(_console, SIGNAL(finished(int)),
    //                 this, SLOT(finished(int)));
    //QObject::connect(_console, SIGNAL(readyReadStandardOutput()),
    //                 this, SLOT(checkConsol()));
}

std::wstring Scripter::recognizeSpeach(const std::string& speachFile)
{
    std::wstring recognized;
    setExecutingScript(_RecognizeSpeachScrit);
    recognized = execute(std::list<std::string>({speachFile}));
    return recognized;
}

void Scripter::createPreview(const std::string &videoFile, double duration)
{
    setExecutingScript(_createPreviewScript);
    int32_t randomPersent = rand() % 100;
    std::string time = "00:00:" + std::to_string(duration*0.25 + randomPersent/100*0.5*duration);

    std::string previewFile = FileWorker::exchangeExtension(videoFile, ".jpeg");

    if (FileWorker::exists(previewFile))
        FileWorker::remove(previewFile);

    execute(std::list<std::string>({videoFile, time, previewFile}));
}

void Scripter::downloadBase()
{
    if (DirWorker::exists(_baseDir))
        setExecutingScript(_downloadBaseScript);
    else
        setExecutingScript(_initBaseScript);
    //qDebug() << _script;
    execute();
}

void Scripter::uploadBase()
{
    setExecutingScript(_uploadBaseScript);
    execute();
}

std::string Scripter::extractAudio(const std::string& videoFile)
{
    std::string rezFileName = FileWorker::exchangeExtension(videoFile, ".wav");
    if (FileWorker::exists(rezFileName))
        FileWorker::remove(rezFileName);

    setExecutingScript(_extractSoundScrit);
    execute(std::list<std::string>({videoFile, rezFileName}));
    rezFileName = DirWorker::absolutePath(rezFileName);
    return rezFileName;
}

void Scripter::normalise(const std::string& wavFile)
{
    setExecutingScript(_normaliseScrit);
    std::string absoluteTmpFile = DirWorker::absolutePath(_tmp_wav);
    execute(std::list<std::string>({wavFile, absoluteTmpFile}));
    FileWorker::remove(wavFile);
    FileWorker::rename(absoluteTmpFile, wavFile);
}

void Scripter::noiseReduse(const std::string& wavFile, const std::string &noiseFile)
{
    setExecutingScript(_noiseReduseScrit);
    std::string absoluteTmpFile = DirWorker::absolutePath(_tmp_wav);
    execute(std::list<std::string>({wavFile, absoluteTmpFile, noiseFile}));
    FileWorker::remove(wavFile);
    FileWorker::rename(absoluteTmpFile, wavFile);
}

std::wstring Scripter::execute()
{
    return execute(std::list<std::string>({}));
}

std::wstring Scripter::execute(const std::list<std::string> &A)
{
    char buffer[65536];
    std::string result = "";
    std::string fullComand = _script;
    for (auto arg : A)
        fullComand += " " + arg;
    std::string currentPath = DirWorker::getCurrentPath();
    fullComand = "sh " + currentPath + fullComand;
    const char* cmd = fullComand.c_str();
    FILE* pipe = _popen(cmd, "r");
    if (!pipe)
        ExceptionHandler::error("Comand pipe is not init for script " + _script);
    try
    {
        while (!feof(pipe))
        {
            if (fgets(buffer, 65536, pipe) != NULL)
                result += buffer;
        }
    }
    catch (...)
    {
        _pclose(pipe);
        ExceptionHandler::error("Comand pipe problems with script " + _script);
    }
    _pclose(pipe);

    std::wstring wresult = StringUtils::s2ws(result);
    return wresult;
}
