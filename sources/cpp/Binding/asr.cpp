#include "asr.h"

ASRScripter::ASRScripter()
{

}

StringUtils::RecognizedString ASRScripter::getRecognized(const std::string& fileName) const
{
    StringUtils::RecognizedString recognized = _fileSpeach.at(fileName);
    return recognized;
}

void ASRScripter::recognize(const std::string& fileName)
{
    std::list<std::string> args;
    args.push_back(fileName);
    startRecognizeScript(args);
}

void ASRScripter::recognize(const std::list<std::string>& fileNames)
{
    std::list<std::string> args;
    for (auto fileName : fileNames)
        args.push_back(fileName);
    startRecognizeScript(args);
}

StringUtils::RecognizedString ASRScripter::handleRecognizedString(const std::wstring& recognized)
{
    if (this->_scripter.executingScriptIs(this->_scripter._RecognizeSpeachScrit_kaldi_en))
        return this->handleRecognizedString_kaldi(recognized);

    return this->handleRecognizedString_sphinx(recognized);
}

StringUtils::RecognizedString ASRScripter::handleRecognizedString_sphinx(const std::wstring& recognized)
{
    StringUtils::RecognizedString str;
    std::list<std::wstring> recognizedStrings = StringUtils::split(recognized, L'\n');
    int32_t i_ignor=0;
    for (std::wstring wordInfoString : recognizedStrings)
    {
        std::list<std::wstring> wordInfo = StringUtils::split(wordInfoString, L' ');
        if (i_ignor == 0)
        {
            i_ignor = wordInfo.size();
            std::wstring recognizedString = StringUtils::join(wordInfo, L' ');
            ExceptionHandler::log(L"Recognized string: \""+ recognizedString + L"\"\n");
            continue;
        }
        std::wstring top = wordInfo.back();
        StringUtils::fixFloatPoint(top);
        double v = std::stod(top); // Без понятия что это за величина, но её пока не учитываем
        wordInfo.pop_back();
        top = wordInfo.back();
        StringUtils::fixFloatPoint(top);
        double wordEnd = std::stod(top);
        wordInfo.pop_back();
        top = wordInfo.back();
        StringUtils::fixFloatPoint(top);
        double wordBegin = std::stod(top);
        wordInfo.pop_back();
        std::wstring wordString = StringUtils::join(wordInfo, L' ');
        StringUtils::RecognizedWord word;
        StringUtils::toRoot(wordString);
        word._recognizedString = wordString;
        word._posBegin = wordBegin;
        word._posEnd = wordEnd;
        str.push_back(word);
        i_ignor--;
    }
    return str;
}

StringUtils::RecognizedString ASRScripter::handleRecognizedString_kaldi(const std::wstring& recognized)
{
    //QStringList outputList = recognized.split(" ", QString::SkipEmptyParts);
    std::list<std::wstring> recognizedStrings = StringUtils::split(recognized, L' ');

    StringUtils::RecognizedString str;

    bool is_firstWord = true;
    for (std::wstring wordString : recognizedStrings)
    {
        if (is_firstWord)
        {
            is_firstWord = false;
            continue;
        }
        StringUtils::RecognizedWord word;
        StringUtils::toRoot(wordString);
        word._recognizedString = wordString;
        word._posBegin = 0;
        word._posEnd = 0;
        str.push_back(word);
    }

    return str;
}


void ASRScripter::startRecognizeScript(const std::list<std::string> &args)
{
    if (args.size() > 1)
        ExceptionHandler::warning("Version is not maintains packedge recognation");

    std::string fileName = args.front();
    std::wstring recognized = _scripter.recognizeSpeach(fileName);
    auto handledStr = handleRecognizedString(recognized);
    _fileSpeach[fileName] = handledStr;
}

