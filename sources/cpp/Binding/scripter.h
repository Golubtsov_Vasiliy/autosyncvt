#ifndef SCRIPTER_H
#define SCRIPTER_H
#include <string>
#include <list>
#include <vector>
#include <assert.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <stdio.h>
#include "../Utils/exceptionHandler.h"
#include "../Utils/fileworker.h"
#include "../Utils/dirworker.h"

#ifdef __GNUC__
    #define _popen popen
    #define _pclose pclose
#endif

// Выполнение скриптов не требующих обратной связи
class Scripter
{
public:
    static const std::string _RecognizeSpeachScrit_sphinx_ru;// = "/scripts/recognizeSpeach_sphinx_ru.sh";
    static const std::string _RecognizeSpeachScrit_sphinx_en;// = "/scripts/recognizeSpeach_sphinx_en.sh";
    static const std::string _RecognizeSpeachScrit_kaldi_en;// = "/scripts/recognizeSpeach_kaldi_en.sh";
    static const std::string _RecognizeSpeachScrit; // = "/scripts/recognizeSpeach.sh";
    static const std::string _noiseReduseScrit;// = QDir::currentPath() + "/Scripts/noiseReduse.sh";
    static const std::string _normaliseScrit;// = QDir::currentPath() + "/Scripts/normalise.sh";
    static const std::string _extractSoundScrit;// = QDir::currentPath() + "/Scripts/extractSound.sh";
    static const std::string _durationSoundScrit;
    static const std::string _downloadBaseScript;// = QDir::currentPath() + "/Scripts/downloadBase.sh";
    static const std::string _initBaseScript;// = QDir::currentPath() + "/Scripts/downloadBase.sh";
    static const std::string _uploadBaseScript;// = QDir::currentPath() + "/Scripts/uploadBase.sh";
    static const std::string _createPreviewScript; // = QDir::currentPath() + "/Scripts/createPreview.sh";
    static const std::string _chanchDiscretScript;
    static const std::string _tmp_wav;// = QDir::currentPath() + "/Scripts/tmp.wav";

    static const std::string _baseDir;// = QDir::currentPath() + "/eduEnReaderBase";

    explicit Scripter();

    // Извлекает аудиодорожку из *.mp4 файла
    // Возвращает имя получившевася файла
    std::string extractAudio(const std::string& videoFile);
    // Распознаёт речь в .wav файле
    std::wstring recognizeSpeach(const std::string& speachFile);
    void noiseReduse(const std::string& wavFile, const std::string& noiseFile);
    void normalise(const std::string& wavFile);
    void downloadBase();
    void uploadBase();
    void createPreview(const std::string& videoFile, double duration);

    // Выполнить скрипт c параметрами А
    std::wstring execute(const std::list<std::string> &A);
    std::wstring execute();
    void setExecutingScript(const std::string& script){ _script = script; }
    //std::string getLastOutput() { return _console->readAllStandardOutput(); }
    //std::string getLastError() { return _console->readAllStandardError(); }
    bool executingScriptIs(const std::string& script) const { return _script == script; }

private:
    std::string _script;
    //std::string _lastConsoleOutput;
};

#endif // SCRIPTER_H
