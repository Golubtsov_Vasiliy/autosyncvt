#ifndef BINDMAKER_H
#define BINDMAKER_H

#include <map>
#include <algorithm>
#include <set>
#include <list>
#include <cstdint>
#include <memory>
#include <wctype.h>
#include "graph.h"
#include "metrics.h"
#include "../textstore.h"
#include "../soundstore.h"
#include "../logic.h"
#include "datapreparation.h"
#include "asr.h"

class BindMaker
{
public:
    typedef std::shared_ptr <BindMaker> PTR;
    static PTR factoryMethod(TextStore::PTR textStore, SoundStore::PTR soundStore, Logic::PTR logic)
    {
        PTR rezPtr = std::shared_ptr <BindMaker> (new BindMaker(textStore, soundStore, logic));
        return rezPtr;
    }
    void setMaxWrongPersent(double maxWrongPersent)
    {
        if (maxWrongPersent > 1.0f)
            ExceptionHandler::critical("Max wrong persent > 1.0");
        if (maxWrongPersent < 0.0f)
            ExceptionHandler::critical("Max wrong persent < 0");
        if (maxWrongPersent > 0.95f)
            ExceptionHandler::warning("I think " + std::to_string(maxWrongPersent*100) + "% wrong persent is very bad recognizer (>95)");
        if (maxWrongPersent < 0.2f)
            ExceptionHandler::warning("I think " + std::to_string(maxWrongPersent*100) + "% wrong persent is very optimistic (<20)");
        _maxWrongPersent = maxWrongPersent;
    }
    void setSplitSize(double splitSize, double diff = 1.0f)
    {
        if (splitSize < 0.0f)
            ExceptionHandler::critical("Slit size < 0.0");
        if (splitSize < 1.5f)
            ExceptionHandler::warning("Slit size is very small (<1.5)");
        if (diff < 0.0f)
            ExceptionHandler::warning("Diff size < 0.0");
        _splitSize = splitSize;
        _diff = diff;
    }
    void setMaxWordError(int32_t maxError){
        if (maxError < 0)
            ExceptionHandler::critical("Max error < 0");
        if (maxError > 10)
            ExceptionHandler::warning("Max word error is very large (>10)");
        _maxWordError = maxError;
    }

    explicit BindMaker(TextStore::PTR textStore, SoundStore::PTR soundStore, Logic::PTR logic);
    BindMaker() = delete;

    std::string getCurState() { return "_curState"; }
    // Подготавливает звуковой файл к распознанию

    void start() { run(); }
    void run(); // запускает процесс биндинга

protected:
    // Делители создоваемые при обработке кандидотов во время правки
    struct Spliter{
        int32_t _textSplit;
        double _soundSplit;
    };
    typedef std::vector <Spliter> SplitVector;

    TextStore::PTR _textStore;
    SoundStore::PTR _soundStore;
    Logic::PTR _logic;

    //std::string _curState;
    double _splitSize;
    double _diff;
    double _maxWrongPersent;
    int32_t _maxWordError; // Максимальная ошибка на втором шаге

    DataPreparation _preparator; // делит данные на куски и подготавливает их к распознованию
    IASR* _asr; // нужен для распознования речи, выделяется в конструкторе
    std::map <std::string, double> _fileBeginOffset; // Нужен для поиска начала части файла, в распозновамом файле
    std::map <std::string, double> _fileEndOffset; // Нужен для поиска конца части файла, в распозновамом файле
    std::map <std::string, StringUtils::RecognizedString> _recognizedStrings; // Нужны для биндинга после подготовки

    std::vector <bool> _handledRecMask; // Маска известных позиций (позиций распознанныйх слов)
    std::vector <bool> _handledCandMasl; // .. (позиций найденых кандидатов)

    std::map <std::string, bool> _processList;
    std::list<std::string> _fileParts;
    // Неплохим вариантом было бы искать промежуточные бинды по функции энергии
    // Более продвинутым ариантом будет посчитать скорость произношения
    // или сколько раз сменилась частота
    // Однако для этого всего придётся считать кареляцию в обратную сторону

    std::map <std::string, std::list<int32_t>> _localMin; // Локальные минимумы кореляционной функции частей файла (слов) к исходному тексту (в словах)
    std::map <std::string, int32_t> _textLength; // Количество символов в каждой распознаной строке

    // ID для графа
    std::map <int32_t, int32_t> _IDToLoacalMin;
    std::map <std::string, std::map <int32_t, int32_t>> _localMinToID;
    std::map <int32_t, std::string> _IDToFileName;

    // Заполняет мапы для графа и сам граф
    void fillInGraph(Graph& g);
    // ID локальный минимум конца, если данный локальный минимум соответствует реальной позиции в тексте
    int32_t getLocalMinEnd(const std::string& fileName, int32_t localMinBegin, const std::string& nextFileName) const;
    // Добавляет бинды  соответствующие списку ID
    void addBinsFromList(const std::list<int32_t> &localMinsIDlist) const;

    // Общее количество локальных минимумов - нужно для формирования графа
    int32_t getLocalMinNumber() const;
    double getFileMid(const std::string& fileName) const;
    double getMid(double begin, double end) const;

    std::string getRespectRecFile(double mid) const;

    void soundPreparetion(double splitSize, double diff = 0.0f); // В случаи фиксированного разбиения
    void soundPreparetion(); // В случаи известного разбиения // Из _logic
    void preparetion();
    void recognizing();
    void binding();

    void sortFileParts();

    double calcProgress() const;
    bool recognizeIsFinished();
    // Используя локальные минимумы функции кореляции,посчитанные при распозновании
    // Cчитаем привязку распознанных фраз к исходному тексту
    // Считаем только файлы из _processList
    void useLocalMinToFind_bind();

    // Пост обработка биндов - после этого этапа мы точно знаем то погрешность позиционировнаия 1? секунда
    void postWordHanding();
    std::vector<std::wstring> getCandidates(int32_t bindNumber) const;
    void makePostSplit(std::vector<Spliter>& rez, const std::vector<std::wstring>& candidates, const std::vector<StringUtils::RecognizedWord>& recognized, int32_t prevC, int32_t nextC, int32_t prevR, int32_t nextR, const std::map<int32_t, int32_t>& candTextPos) const;
    void findUniq(const std::vector<std::wstring>& candidates, const std::vector<StringUtils::RecognizedWord>& recognized, int32_t prevC, int32_t nextC, int32_t prevR, int32_t nextR, int32_t &uniqC, int32_t &uniqR) const;
    std::map<int32_t, int32_t> getCandidatesPos(int32_t firstPos, const std::vector<std::wstring>& candidates) const;
    void handleBindsUsingSplits(const SplitVector &spliters);
    void sortSplitVector(SplitVector &spliters) const;
    // Так как нас интереисует на данном этапе наиболее точное решение - удаляем все источники колизий
    void destroyColisions(SplitVector &splits) const;
    // если несколько слов определены как произнесённые в одну секкунду объеденяем разделители
    void uniteClosest(SplitVector &splits) const;


    void addCandidatesFromBind(std::vector<std::wstring>& candidates, const Logic::Bind& bind, int32_t wordsNumber) const;

    // TODO Это явно можно оптимизировать (Будет жрать память)
    //void makeSplit(splitVector&, const std::string& fileName, int32_t beginWord, int32_t endWord, double beginSound, double endSound);

    // Ищет реальную позицию кандидата, используя _textList и _textPos
    int32_t getRealWordPos(const Logic::Bind&, int32_t offsetPos);

    // Вычисляем точки локальных минимумов
    // maxWrongPersent = [0..1.0f] - максимально допустимая степень непохожести.
    // При 0.75 достаточно 1 правильного слова из 4 на своём месте.
    // При 0.25 - допускаеться только 4 неверно распознаных слова из 16.
    void calcLocalMin(const std::string& partFileName, double maxWrongPersent);

    // Сейчас считаеться кореляция распознонова к тексту
    // Берутся части из текста размером с распознаные
    // Возможно стоит делать наоборот, по скольку часть текста может быть не распознано
    // При этом маловероятно что после очистки от шума будет распознано что то лишнее
    // однако на данном этапе мы не избавляемся от звуков не являющихся голосом
    // Плюс так мы можем добавлять лишнее в тексте, оно будет проигнорировано
    std::list <int32_t> getCorelationFunc(const std::list<wstring> &recognizedString) const;

    // Нужна для поиска локального минимума кореляии текстов
    int32_t getDistance(const std::list<std::wstring>& firstSeqence, const std::list<std::wstring>& secondSeqence) const;
    //void deleteNotAlpha(wstring &str);

    void clear();
};

#endif // BINDMAKER_H
