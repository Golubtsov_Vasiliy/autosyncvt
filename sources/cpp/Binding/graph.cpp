#include "graph.h"

Graph::Graph(int32_t V)
{
    this->V = V;
    adj.resize(V);
}

void Graph::addEdge(int32_t u, int32_t v)
{
    AdjListNode node(v, 1);
    adj[u].push_back(node); // Add v to u's list
}

// A recursive function used by longestPath. See below link for details
// http://www.geeksforgeeks.org/topological-sorting/
void Graph::topologicalSortUtil(int32_t v, std::vector <bool>& visited, std::stack<int32_t> &Stack)
{
    // Mark the current node as visited
    visited[v] = true;

    // Recur for all the vertices adjacent to this vertex
    std::list<AdjListNode>::iterator i;
    for (i = adj[v].begin(); i != adj[v].end(); ++i)
    {
        AdjListNode node = *i;
        if (!visited[node.getV()])
            topologicalSortUtil(node.getV(), visited, Stack);
    }

    // Push current vertex to stack which stores topological sort
    Stack.push(v);
}

// The function to find longest distances from a given vertex. It uses
// recursive topologicalSortUtil() to get topological sorting.
std::list <int32_t> Graph::longestPath(int32_t s)
{
    std::map<int32_t, int32_t> prev; // говорит из какой вершины мы пришли

    for (std::int32_t i = 0; i < V; i++)
        prev[i] = NINF;

    std::stack<int32_t> Stack;

    // Mark all the vertices as not visited
    std::vector <bool> visited;
    visited.resize(V);
    for (int32_t i = 0; i < V; i++)
        visited[i] = false;

    // Call the recursive helper function to store Topological Sort
    // starting from all vertices one by one
    for (int32_t i = 0; i < V; i++)
        if (visited[i] == false)
            topologicalSortUtil(i, visited, Stack);

    // Initialize distances to all vertices as infinite and distance
    // to source as 0
    std::vector <int32_t> dist;
    dist.resize(V);
    for (int32_t i = 0; i < V; i++)
        dist[i] = NINF;
    dist[s] = 0;

    // Process vertices in topological order
    while (Stack.empty() == false)
    {
        // Get the next vertex from topological order
        int32_t u = Stack.top();
        Stack.pop();

        // Update distances of all adjacent vertices
        std::list<AdjListNode>::iterator i;
        if (dist[u] != NINF)
        {
          for (i = adj[u].begin(); i != adj[u].end(); ++i)
             if (dist[i->getV()] < dist[u] + i->getWeight())
             {
                 prev[i->getV()] = u;
                 dist[i->getV()] = dist[u] + i->getWeight();
             }
        }
    }

    // Print the calculated longest distances
    std::list <int32_t> path = getMaxPath(dist, prev);
    return path;
}

std::list <int32_t> Graph::getMaxPath(std::vector <int32_t> dist, std::map<int32_t, int32_t> prev)
{
    std::list <int32_t> path;

    int32_t cur = V-1;
    int32_t maxDistance = 0;
    for (int32_t c=0; c<V; c++)
        if (maxDistance < dist[c])
        {
            maxDistance = dist[c];
            cur = c;
        }

    while (cur != NINF)
    {
        path.push_front(cur);
        cur = prev[cur];
        assert(path.size() <= V);
    }
    return path;
}

std::list<int32_t> Graph::longestPath(std::list <int32_t> fatherList)
{
    // TODO Тут нужно найти точки для которых нет отцов и искать уже по ним
    std::list <int32_t> rezPath;
    for (int32_t i : fatherList)
    {
        std::list <int32_t> curPath = longestPath(i);
        if (curPath.size() > rezPath.size())
            rezPath = curPath;
        //double curPersent = (double)i / (double)fatherList.size();
        //emit process(curPersent);
    }

    return rezPath;
}
