#ifndef METRICS_H
#define METRICS_H

#include <string>
#include <vector>
#include <list>
#include <algorithm>

// Статический класс реализующий метрики
class Metrics
{
public:
    Metrics() = delete;
    static int32_t getLevenshteinDistance(const std::list<std::wstring> &s1, const std::list<std::wstring> &sr2);
    // Используеться в getDistance, проверяет равенство слов
    static bool isEqils(const std::wstring& string1, const std::wstring& string2);
private:
};

#endif // METRICS_H
