#ifndef LOGIC_H
#define LOGIC_H

#include <map>
#include <list>
#include <vector>
#include <iostream>
#include <cstdint>
#include <memory>
#include <fstream>
#include "Binding/asr.h"
#include "soundfragment.h"
#include "textfragment.h"
#include "Utils/stringutils.h"
#include "Utils/fileworker.h"
#include "baselogic.h"

class Logic : public BaseLogic
{
public:
    typedef std::shared_ptr <Logic> PTR;
    static PTR factoryMethod()
    {
        PTR rezPtr = std::shared_ptr<Logic>(new Logic());
        return rezPtr;
    }

    // Нужна для поправки биндов (postWordHanding)
    // Возвращает слова в бинде и рядом с ним с учётом ошибки
    //std::vector<std::wstring> getWordCandidatesFromPosition(int32_t beginPos, int32_t endPos, int32_t maxWordError) const;
    // Нужна чтобы получать из кандидатов реальные позиции в тексте
    //int32_t getRealWordPos(std::vector<Bind>::iterator bind, int32_t pos);

    // Логически обрабатывает бинлы
    void bindLogicHanding();

    // Методы отвечающие за сохранение/чтение файлов
    // В файл биндов помимо названия файлов и самих биндов сохраняется MD5
    // Файлы должны хранится в одной директории, используются относительные пути
    //void writeInFile(const std::string& fileName);
    void save();

    // pos - номер бинда перед которым будет вставлен бинд
    void makeBind(TextFragment::PTR text, SoundFragment::PTR sound);
    void addRecognizedInBind(const StringUtils::RecognizedString& rec, int32_t bindNumber = -1);
    void addRecognizedInBinds(); // Добавляет информацию о том какие слова распознанны в бинде
    StringUtils::RecognizedString recognizedInBind(int32_t bindNumber) const;
    StringUtils::RecognizedString getRecognizedString(const std::string &fileName) const;
    StringUtils::RecognizedString getRecognizedIn(double begin, double end) const;

    // fileName = ID
    bool haveRecognizedString();
    std::map <std::string, StringUtils::RecognizedString> getRecognizedStrings() const;
    std::map <std::string, double> getRecognizedStringBeginList() const;
    std::map <std::string, double> getRecognizedStringEndList() const;

protected:
    Logic(void);

};


#endif // LOGIC_H
