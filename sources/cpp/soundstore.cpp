#include "soundstore.h"

SoundStore::SoundStore()
{

}

void SoundStore::setFileUrl(const std::string& url)
{
    _lastOpenedUrl = DirWorker::absolutePath(url);
    if (FileWorker::exists(_lastOpenedUrl) == false)
        _lastOpenedUrl = url;
    if (FileWorker::exists(_lastOpenedUrl) == false)
        ExceptionHandler::error("We can\t find file " + url + " or " + _lastOpenedUrl);
}

std::string SoundStore::fileUrl() const
{
    return _lastOpenedUrl;
}

double SoundStore::fullDuration()
{
    return (double)duration() / 1000;
}

int64_t SoundStore::duration() const
{

    return -1;
}
