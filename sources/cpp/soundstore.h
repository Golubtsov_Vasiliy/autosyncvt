#ifndef SOUNDSTORE_H
#define SOUNDSTORE_H
#include <string>
#include <cstdint>

#include "Utils/fileworker.h"
#include "store.h"

class SoundStore : public Store <SoundStore>
{
public:
    SoundStore();
    virtual ~SoundStore() {
       // qDebug() << "~SoundStore()";
    }
    void setFileUrl(const std::string& url);
    std::string fileUrl() const override;
    double fullDuration();

protected:
    friend class Store <SoundStore>;
    std::string _lastOpenedUrl;
    int64_t _lastOpenedDuration;

    int64_t duration() const;
};

#endif // SOUNDSTORE_H
