#include "textstore.h"
#include <fstream>
#include <iostream>
#include <string>
#include <locale>
//#include <codecvt> // from Clang

TextStore::TextStore()
{

}

int32_t TextStore::wordListSize() const
{
    int32_t size = _textWordList.size();
    return size;
}

int32_t TextStore::wordRealPos(int32_t i) const
{
    int32_t rez = _wordPosToTextPos.at(i);
    return rez;
}

int32_t TextStore::wordNumberOnPos(int32_t i) const
{
    int32_t rez = _charPosToWordPos.at(i);
    return rez;
}

std::wstring TextStore::word(int32_t i) const
{
    std::wstring rez = _textWordList.at(i);
    return rez;
}

std::wstring TextStore::wordOnPos(int32_t p) const
{
    int32_t wordPos = _wordPosToTextPos.at(p);
    std::wstring rez = _textWordList.at(wordPos);
    return rez;
}

void TextStore::prepeareWordSplit()
{
    if (_fullDocText.empty())
        ExceptionHandler::warning("Empty string split trying");
    int32_t leftSpacePos = StringUtils::findFirstAlpha(_fullDocText);
    if (iswalpha(_fullDocText.at(0)))
        leftSpacePos = 0;
    int32_t wordNumber = _fullDocText.size();
    _textWordList.reserve(wordNumber);
    while (leftSpacePos != std::wstring::npos)
    {
        int32_t rightSpacePos = StringUtils::findFirstNotAlpha(_fullDocText, leftSpacePos+1);
        if (rightSpacePos == std::wstring::npos )
            return;
        auto nextWordLength = rightSpacePos-leftSpacePos;
        std::wstring nextWord = _fullDocText.substr(leftSpacePos, nextWordLength);
        StringUtils::toRoot(nextWord);
        _textWordList.push_back(nextWord);
        //_textWordPos.push_back(leftSpacePos);
        int32_t curWordNumber = _textWordList.size() - 1;
        _charPosToWordPos[leftSpacePos] = curWordNumber;
        _wordPosToTextPos[curWordNumber] = leftSpacePos;
        leftSpacePos = StringUtils::findFirstAlpha(_fullDocText, rightSpacePos + 1);
    }
}

std::wstring TextStore::getString(int32_t begin, int32_t end) const
{
    int32_t rezLength = end - begin;
    return _fullDocText.substr(begin, rezLength);
}

std::wstring TextStore::getString() const
{
    return _fullDocText;
}

std::string TextStore::fileUrl() const
{
    return _curFileUrl;
}

std::wstring TextStore::readFile(const std::string& fileName)
{
    std::wifstream fs;
    std::locale loc("");
    fs.imbue(loc);
    fs.open(fileName, std::ios::binary);
    if (fs.fail())
        ExceptionHandler::critical("Failed to open file " + fileName);
    fs.seekg(0, std::ios::end);
    size_t i = fs.tellg();
    wchar_t* buf = new wchar_t[i];
    fs.seekg(0, std::ios::beg);
    fs.read(buf, i);
    fs.close();
    std::wstring s;
    s.assign(buf, i);
    delete [] buf;
    return s;
}


void TextStore::setFileUrl(const std::string &newUrl)
{
    _curFileUrl = newUrl;
    _fullDocText = readFile(newUrl);
}

int32_t TextStore::length() const
{
    return _fullDocText.length();
}

bool TextStore::posIsCorrect(int32_t curPos) const
{
    int32_t length = this->length();
    if (curPos >= 0 || curPos < length)
        return true;
    return false;
}
