#include "baselogic.h"

BaseLogic::BaseLogic()
{

}

void BaseLogic::writeInFile(TextStore::PTR textStore, SoundStore::PTR soundStore)
{
    writeInFile(_curBndFileName, textStore, soundStore);
}

void BaseLogic::writeInFile(const std::string& fileName, TextStore::PTR textStore, SoundStore::PTR soundStore)
{
    std::wstring textStoreString;
    std::wstring soundStoreString;
    std::string curPath = DirWorker::absolutePath(fileName);
    curPath = FileWorker::pathToFile(curPath);
    _curBndFileName = fileName;
    if (textStore)
    {
        std::string tS = textStore->toString(curPath);;
        textStoreString = std::wstring(tS.begin(), tS.end());
        _lastOpenedTextStore = textStore;
    }
    if (soundStore)
    {
        std::string sS = soundStore->toString(curPath);
        soundStoreString = std::wstring(sS.begin(), sS.end());
        _lastOpenedSoundStore = soundStore;
    }
    std::wofstream file;
    file.open(fileName);
    std::locale loc("");
    file.imbue(loc);
    if (file.is_open() == false)
    {
        ExceptionHandler::error("File " + fileName + " not open");
        return;
    }
    file << par_TextStore << " " << textStoreString << "\n";
    file << par_SoundStore << " " << soundStoreString << "\n";
    for (auto bind : _bindVector)
    {
        std::wstring curStringBind = toString(bind);
        file << par_Bind << " " << curStringBind << "\n";
    }

    for (auto recognizedString : _recognizedStrings)
    {
        file << par_RecognizedString << " ";
        std::wstring recognizedStringString = toString(recognizedString.second);
        file << recognizedStringString << "\n";
    }
    file.close();
}

void BaseLogic::readFromFile(const std::string &fileName, TextStore::PTR textStore, SoundStore::PTR soundStore)
{
    std::wstring textStoreString;
    std::wstring soundStoreString;
    std::list<std::wstring> bindListString;
    std::list<std::wstring> recognizedStrings;
    clear(true);

    _curBndFileName = fileName;
    std::string curPath = FileWorker::pathToFile(fileName);
    std::wifstream file;
    std::locale loc("");
    file.imbue(loc);
    file.open(fileName);

    if (!file.is_open())
    {
        ExceptionHandler::error("File is not open");
        return;
    }
    std::wstring par_buff;
    std::wstring value_buff;
    while (file)
    {
        file >> par_buff;
        std::getline(file, value_buff);
        StringUtils::trim(value_buff);
        StringUtils::fixFloatPoint(value_buff);
        if (par_buff == par_Bind)
            bindListString.push_back(value_buff);
        else if (par_buff == par_RecognizedString)
            recognizedStrings.push_back(value_buff);
        else if (par_buff == par_SoundStore)
            soundStoreString = value_buff;
        else if (par_buff == par_TextStore)
            textStoreString = value_buff;
    }

    if (textStore)
    {
        std::string tSS = std::string(textStoreString.begin(), textStoreString.end());
        textStore->fromString(tSS, curPath);
    }
    _lastOpenedTextStore = textStore;
    if (soundStore)
    {
        std::string sSS = std::string(soundStoreString.begin(), soundStoreString.end());
        soundStore->fromString(sSS, curPath);
    }
    else
        soundStore = SoundStore::factoryMethod();
    _lastOpenedSoundStore = soundStore;

    for (auto bindString : bindListString)
    {
        Bind nextBind;
        fromString(nextBind, bindString, soundStore, textStore);
        addInBindList(nextBind);
    }
    for (auto recognizedStr : recognizedStrings)
    {
        StringUtils::RecognizedString rec;
        fromString(rec, recognizedStr);
        double begin = rec.front()._posBegin;
        double end = rec.front()._posEnd;
        addRecognizedString(rec, begin, end);
    }

    file.close();
}

void BaseLogic::clear(bool clearRecognized)
{
    _bindVector.clear();
    if (clearRecognized)
    {
        _recognizedStringPosBegin.clear();
        _recognizedStringPosEnd.clear();
        _recognizedStrings.clear();
        _recognizedInBind.clear();
    }
}

void BaseLogic::addRecognizedString(const StringUtils::RecognizedString& recognizedString, SoundFragment::PTR sound, std::string fileName)
{
    double soundBegin = sound->begin();
    double soundEnd = sound->end();
    addRecognizedString(recognizedString, soundBegin, soundEnd, fileName);
}

void BaseLogic::addRecognizedString(const StringUtils::RecognizedString& recognizedString, double beginSound, double endSound, std::string fileName)
{
    if (fileName.length() == 0)
    {
        int ID = _recognizedStrings.size();
        fileName = std::to_string(ID);
    }
    _recognizedStrings[fileName] = recognizedString;
    _recognizedStringPosBegin[fileName] = beginSound;
    _recognizedStringPosEnd[fileName] = endSound;
}

std::wstring BaseLogic::toString(const Bind& bind) const
{
    auto bindText = bind.text;
    int32_t textBegin = bindText->begin();
    int32_t textEnd = bindText->end();
    auto bindSound = bind.sound;
    double soundBegin = bindSound->begin();
    double soundEnd = bindSound->end();

    std::wstring rez;
    rez += std::to_wstring(textBegin) + L" " + std::to_wstring(textEnd) + L" "
              + std::to_wstring(soundBegin) + L" " + std::to_wstring(soundEnd);
    return rez;
}

void BaseLogic::fromString(Bind& bind, const std::wstring &bindString, SoundStore::PTR sound, TextStore::PTR text) const
{
    std::list <std::wstring> bindStrList = StringUtils::split(bindString, L' ');

    auto curEl = bindStrList.begin();
    int32_t textBegin = std::stoi(*curEl);
    curEl++;
    int32_t textEnd = std::stoi(*curEl);
    curEl++;
    double soundBegin = std::stod(*curEl);
    curEl++;
    double soundEnd = std::stod(*curEl);

    auto soundFragment = SoundFragment::factoryMethod(soundBegin, soundEnd, sound);
    auto textFragment = TextFragment::factoryMethod(textBegin, textEnd, text);

    bind.sound = soundFragment;
    bind.text = textFragment;
}

// format "word word" time_begin time_end
std::wstring BaseLogic::toString(const StringUtils::RecognizedString &str) const
{
    std::wstring rezString;
    for (StringUtils::RecognizedWord word : str)
    {
        rezString += L"\"" + word._recognizedString + L"\" ";
        rezString += std::to_wstring(word._posBegin);
        rezString += L" ";
        rezString += std::to_wstring(word._posEnd);
        rezString += L" ";
    }
    return rezString;
}

// format "word word" time_begin time_end
void BaseLogic::fromString(StringUtils::RecognizedString& str, const std::wstring recognizedStringString) const
{
    str.clear();
    std::list<std::wstring> recStr = StringUtils::split(recognizedStringString, ' ');
    for (auto i_str = recStr.begin(); i_str!=recStr.end();)
    {
        StringUtils::RecognizedWord word;
        std::wstring buff = *i_str;
        while (i_str != recStr.end() && (*i_str).back() != '\"')
        {
            i_str++;
            buff+=*i_str;
        }
        buff = buff.substr(1, buff.length() - 2);
        word._recognizedString = buff;
        i_str++;
        std::wstring beginString = *i_str;
        word._posBegin = std::stof(beginString);
        i_str++;
        std::wstring endString = *i_str;
        word._posEnd= std::stof(endString);
        str.push_back(word);
        i_str++;
    }
}

bool BaseLogic::isEquils(const Bind& left, const Bind& right) const {
    auto l_sound = left.sound;
    auto l_text = left.text;
    auto r_sound = right.sound;
    auto r_text = right.text;
    if (!r_text || !r_sound || !l_text || !l_sound)
        return false;
    return l_text->begin() == r_text->begin() && l_text->end() == r_text->end() &&
           l_sound->begin() == r_sound->begin() && l_sound->end() == r_sound->end();
}

BaseLogic::Bind BaseLogic::summ(const Bind& left, const Bind& right) const
{
    if (isEquils(left, zeroBind))
        return right;
    if (isEquils(right, zeroBind))
        return left;

    Bind summBind;
    TextFragment::PTR summText;
    SoundFragment::PTR summSound;
    TextFragment::PTR leftText = left.text;
    TextFragment::PTR rightText = right.text;
    SoundFragment::PTR leftSound = left.sound;
    SoundFragment::PTR rightSound = right.sound;

    //summText = TextFragment::summ(leftText, rightText);
    summSound = SoundFragment::summ(leftSound, rightSound);

    summBind.sound = summSound;
    summBind.text = summText;

    return summBind;
}

void BaseLogic::addInBindList(const Bind& bind)
{
    _bindVector.push_back(bind);
}

std::vector <BaseLogic::Bind> BaseLogic::getBindVector() const
{
    return _bindVector;
}

std::string BaseLogic::getCurBndFileName() const
{
    return _curBndFileName;
}

int32_t BaseLogic::getBindVectorSize() const
{
    return _bindVector.size();
}

int32_t BaseLogic::getBindTextPos(int32_t pos) const
{
    Bind bind = _bindVector.at(pos);
    TextFragment::PTR text = bind.text;
    int32_t textPos = text->begin();
    return textPos;
}

BaseLogic::Bind BaseLogic::getBind(int32_t bindNumber) const
{
    return _bindVector.at(bindNumber);
}
