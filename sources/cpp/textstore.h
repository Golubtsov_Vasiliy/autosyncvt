#ifndef TEXTSTORE_H
#define TEXTSTORE_H

#include <cstdint>
#include <fstream>
#include <sstream>
#include <map>
//#include <codecvt> // В linux для Русского языка поставить gcc (>4.10), gcc-locate, clang и libc++
#include "store.h"

class TextStore : public Store <TextStore>
{
public:
    TextStore();
    virtual ~TextStore(){}

    std::wstring getString(int32_t begin, int32_t end) const;
    std::wstring getString() const;

    std::string fileUrl() const override;
    void setFileUrl(const std::string &newUrl) override;

    int32_t length() const;
    bool posIsCorrect(int32_t curPos) const;

    bool empty() const { return _fullDocText.empty(); }

    void prepeareWordSplit();

    // Работа со словами
    int32_t wordListSize() const;
    int32_t wordRealPos(int32_t i) const;
    int32_t wordNumberOnPos(int32_t p) const;
    std::wstring word(int32_t i) const;
    std::wstring wordOnPos(int32_t p) const;

protected:
    std::wstring _fullDocText;
    std::string _curFileUrl;

    // Список слов текста и их позции в исходном тексте
    std::vector<std::wstring> _textWordList;
    //std::vector<int32_t> _textPos; // сопостовляет позицию в _textList с настоящей позицией в тексте
    std::map <int32_t, int32_t> _charPosToWordPos; // по позиции в тексте возвращает номер слова
    std::map <int32_t, int32_t> _wordPosToTextPos;

    std::wstring readFile(const std::string& fileName);
};




#endif // TEXTSTORE_H
