#include <iostream>
#include <locale.h>
#include <fstream>
#include "cpp/logic.h"
#include "cpp/Binding/bindmaker.h"

int main(int argc, char *argv[])
{
    //locale::global( locale( "" ) );
    setlocale (LC_ALL, "Russian"); // Rus и Russian - отличаются форматом записи числа?
    DirWorker::initDurWorker(argc, argv);

  /*  Logic::PTR logic = Logic::factoryMethod();
    SoundStore::PTR sound = SoundStore::factoryMethod();
    TextStore::PTR text = TextStore::factoryMethod();

    sound->setFileUrl("test.mp4");
    text->setFileUrl("test.txt");

    //logic->readFromFile("test.bnd", text, sound);
    BindMaker::PTR binder = BindMaker::factoryMethod(text, sound, logic);

    binder->setMaxWrongPersent(0.85f);
    binder->setSplitSize(4.0f, 2.0f);
    binder->setMaxWordError(3);
    binder->run();

    logic->writeInFile("test.bnd", text, sound);

    return 0;*/

    std::string videoFileListFile = "./files.dat";
    if (argc > 1)
        videoFileListFile = argv[1];

    std::ifstream testList(videoFileListFile);
    std::string nextFile;
    while (testList >> nextFile)
    {
        Logic::PTR logic = Logic::factoryMethod();
        SoundStore::PTR sound = SoundStore::factoryMethod();
        TextStore::PTR text = TextStore::factoryMethod();

        std::string videoFile = "DATA" + StringUtils::curSplitDirString + nextFile;
        std::string textFile = FileWorker::exchangeExtension(videoFile, ".txt");
        std::string outBindFile = FileWorker::exchangeExtension(videoFile, ".bnd");

        //if (FileWorker::exists(outBindFile))
        //    continue;

        videoFile = DirWorker::absolutePath(videoFile);
        textFile = DirWorker::absolutePath(textFile);

        sound->setFileUrl(videoFile);
        text->setFileUrl(textFile);

        //logic->readFromFile(outBindFile, text, sound);
        BindMaker::PTR binder = BindMaker::factoryMethod(text, sound, logic);

        binder->setMaxWrongPersent(0.85f);
        binder->setSplitSize(4.0f, 2.0f);
        binder->setMaxWordError(3);
        binder->run();

        logic->writeInFile(outBindFile, text, sound);
    }

    std::cout << "Ok. Well done.";
    return 0;
}
