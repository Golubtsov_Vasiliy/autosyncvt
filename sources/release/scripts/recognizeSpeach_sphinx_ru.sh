#!/bin/bash

modelPath="/home/gva/models/zero_ru_cont_8k_v3/"

#echo "${modelPath}zero_ru.cd_cont_4000"
#echo "${modelPath}ru.dic"

pocketsphinx_continuous -infile $1 -hmm "${modelPath}zero_ru.cd_cont_4000" -dict "${modelPath}ru.dic" -lm "${modelPath}ru.lm" -remove_noise no -time yes -bestpath yes  -logfn "${1}_log" | grep -v "<" | grep -v "\["
