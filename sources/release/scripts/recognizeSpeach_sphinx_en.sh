#!/bin/bash


modelPath="/home/gva/models/cmusphinx-en-us-8khz-5.2/"

pocketsphinx_continuous -infile $1 -lm "${modelPath}en.lm" -dict "${modelPath}en-us.dict" -hmm "${modelPath}" -remove_noise no -time yes -bestpath yes  -logfn "${1}_log" | grep -v "<" | grep -v "\["
