#!/bin/bash

pocketsphinx_continuous -infile $1 -time yes -bestpath yes  -logfn "${1}_log" | grep -v "<" | grep -v "\["
