QT -= core
QT -= gui

CONFIG += c++11

TARGET = autoSyncVT
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

#manjaro || suse
LIBS += -lsndfile

SOURCES += main.cpp \
    cpp/fragment.cpp \
    cpp/logic.cpp \
    cpp/soundfragment.cpp \
    cpp/soundstore.cpp \
    cpp/store.cpp \
    cpp/textfragment.cpp \
    cpp/textstore.cpp \
    cpp/Binding/asr.cpp \
    cpp/Binding/bindmaker.cpp \
    cpp/Binding/datapreparation.cpp \
    cpp/Binding/graph.cpp \
    cpp/Binding/metrics.cpp \
    cpp/Binding/scripter.cpp \
    cpp/Binding/wavworker.cpp \
    cpp/Utils/exeptionhandler.cpp \
    cpp/Utils/dirworker.cpp \
    cpp/Utils/stringutils.cpp \
    cpp/Utils/fileworker.cpp \
    cpp/baselogic.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    cpp/fragment.h \
    cpp/logic.h \
    cpp/soundfragment.h \
    cpp/soundstore.h \
    cpp/store.h \
    cpp/textfragment.h \
    cpp/textstore.h \
    cpp/Binding/asr.h \
    cpp/Binding/bindmaker.h \
    cpp/Binding/datapreparation.h \
    cpp/Binding/graph.h \
    cpp/Binding/metrics.h \
    cpp/Binding/scripter.h \
    cpp/Binding/wavworker.h \
    cpp/Utils/exceptionHandler.h \
    cpp/Utils/dirworker.h \
    cpp/Utils/stringutils.h \
    cpp/Utils/fileworker.h \
    cpp/baselogic.h

